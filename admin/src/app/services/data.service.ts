import { AdminUsuarioInteface } from "src/app/models/adminUsuario.interface";
import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { config } from "./config";

import { AtividadeInterface } from "../models/atividade.interface";
import { MaterialInterface } from "../models/material.interface";
import { ObjetivoInterface } from "../models/objetivo.interface";
import { UsuarioInteface } from "../models/usuario.interface";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { ToastService } from "./toast.service";
@Injectable()
export class DataService {
  constructor(
    public db: AngularFireDatabase,
    public dbStore: AngularFirestore,
    private afMessaging: AngularFireMessaging,
    private toastSrv: ToastService
  ) {}
  get getAtividadeList() {
    return this.db
      .list<AtividadeInterface>(/* config.url.base + */ config.url.dbAtividades)
      .valueChanges();
  }

  get getUsuariosList() {
    return this.db
      .list<UsuarioInteface>(/* config.url.base + */ config.url.dbUser)
      .valueChanges();
  }
  get getMaterialList() {
    return this.db
      .list<MaterialInterface>(/* config.url.base + */ config.url.dbMateriais)
      .valueChanges();
  }
  get getObjetivoList() {
    return this.db
      .list<ObjetivoInterface>(/* config.url.base + */ config.url.dbObjetivos)
      .valueChanges();
  }
  get getAdministradores() {
    return this.db
      .list<AdminUsuarioInteface>(/* config.url.base + */ config.url.dbAdmins)
      .valueChanges();
  }
  public saveTo(ev: any, path: string) {
    this.db
      .list(/* config.url.base +  */path)
      .set(ev.id, ev)
      .then((res) => {
        switch (path) {
          case config.url.dbAtividades:
            this.toastSrv.showToastSucess("Atividade salva com sucesso");
            break;
          case config.url.dbUser:
            this.toastSrv.showToastSucess("Usuario salvo com sucesso");
            break;
          case config.url.dbMateriais:
            this.toastSrv.showToastSucess("Material salvo com sucesso");
            break;
          case config.url.dbObjetivos:
            this.toastSrv.showToastSucess("Objetivo salvo com sucesso");
            break;
        }
      })
      .catch((err) => {
        this.toastSrv.showToastError(
          "Não foi possivel concluir a sua operação",
          "por favor, tente novamente"
        );
      });
  }

  public saveToFireStore(ev: any, path: string) {
    this.dbStore
      .collection(/* config.url.base +  */path)
      .doc(ev.id).set(ev)
      .then((res) => {
        switch (path) {
          case config.url.dbAtividades:
            this.toastSrv.showToastSucess("Atividade salva com sucesso");
            break;
          case config.url.dbUser:
            this.toastSrv.showToastSucess("Usuario salvo com sucesso");
            break;
          case config.url.dbMateriais:
            this.toastSrv.showToastSucess("Material salvo com sucesso");
            break;
          case config.url.dbObjetivos:
            this.toastSrv.showToastSucess("Objetivo salvo com sucesso");
            break;
        }
      })
      .catch((err) => {
        console.log(err)
        this.toastSrv.showToastError(
          "Não foi possivel concluir a sua operação",
          "por favor, tente novamente"
        );
      });
  }

  getSpecificUser(userID) {
    return this.db
      .object<UsuarioInteface>(
       /*  config.url.base + */ config.url.dbUser + "/" + userID
      )
      .valueChanges();
  }

  set deleteFromDB(ev: { arr: any[]; path: string }) {
    ev.arr.forEach((element) => {
      this.db.list(/* config.url.base + */ ev.path).remove(element.id);
    });
  }

  public deleteFolderFromDB(url: string) {
    this.db.list(url).remove();
    this.dbStore.collection(url)
  }

  sendNotification() {}
}
