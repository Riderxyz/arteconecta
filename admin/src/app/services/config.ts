import { ColDef } from 'ag-grid-community';
import moment from 'moment';
export const config = {
  rxjsCentralKeys: {
    GridResize: 'gridResize',
    GridReady: 'gridReady',
    ChangeToMobile: 'changeToMobile',
    ChangeToWeb: 'changeToWeb',
    pwaUpdateRequest: 'pwaUpdateRequest',
    lastUpdate: 'lastUpdate',
    openSideMenu: 'openSideMenu',
    closeSideMenu: 'closeSideMenu',
    onGridFilter: 'onGridFilter'
  },
  getRandomId: () => {
    return Math.random()
      .toString(36)
      .substr(2, 9);
  },
  url: {
    base: '/ArteConecta',
    dbUser: '/usuario',
    dbMateriais: '/material',
    dbObjetivos: '/objetivo',
    dbAtividades: '/atividades',
    dbAdmins: '/administradores',
    pushNotificationAPI:
      'https://us-central1-arteconecta-27292.cloudfunctions.net/SendNotification'
  },
  columnDef: {
    atividadeGrid: [
      {
        headerName: 'titulo'.toLocaleUpperCase(),
        field: 'titulo',
        width: 30,
        checkboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      },
      {
        headerName: 'Tipo'.toLocaleUpperCase(),
        field: 'tipo',
        // cellRenderer: params => {
        //   return Number(params.data.tipo) !== 1 ? 'Proposta':'VideoAula';
        // },
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'Gratis'.toLocaleUpperCase(),
        field: 'free',
        cellRenderer: params => {
          return params.data.free == 1 && params.data.publicado == 1? 'Sim':'Não';
        },
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'criado por'.toLocaleUpperCase(),
        field: 'criadopor',
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'Criado em'.toLocaleUpperCase(),
        field: 'data_criacao',
        cellRenderer: params => {
          if (params.data.data_criacao !== undefined) {
            const retornoComentario =
              `
               <span style='text-overflow: ellipsis'>
  ` +
              moment(params.data.update_data).format('L') +
              `
  </span>
  `;
            return retornoComentario;
          } else {
            return ' ';
          }
        },
        width: 5
      },
      {
        headerName: 'Ultima Atualização'.toLocaleUpperCase(),
        field: 'update_data',
        cellRenderer: params => {
          const retornoComentario =
            `
          <span style='text-overflow: ellipsis'>
          ` +
            moment(params.data.update_data).format('L') +
            `
          </span>
          `;
          return retornoComentario;
        },
        width: 10,
        autoHeight: true
      },
      {
        headerName: 'Comentarios'.toLocaleUpperCase(),
        field: 'comentarios',
        width: 10,
        autoHeight: true,
        cellRenderer: params => {
          if (params.data.comentarios !== undefined) {
            const retornoComentario =
              `
            <span style='text-overflow: ellipsis'>
            ` +
              params.data.comentarios +
              `
            </span>
            `;
            return retornoComentario;
          } else {
            return ' ';
          }
        }
      },
      {
        headerName: 'url do Video'.toLocaleUpperCase(),
        field: 'videourl',
        width: 10,
        autoHeight: true
      },
      {
        headerName: 'ID',
        field: 'id',
        maxWidth: 0,
        hide: true
      }
    ] as ColDef[],
    usuarioGrid: [
      {
        headerName: 'nome'.toLocaleUpperCase(),
        field: 'nome',
        width: 30,
        checkboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      },
      {
        headerName: 'email'.toLocaleUpperCase(),
        field: 'emailPrincipal',
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'telefone'.toLocaleUpperCase(),
        field: 'telefone',
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'cidade'.toLocaleUpperCase(),
        field: 'endereco_cidade',
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'uf'.toLocaleUpperCase(),
        field: 'endereco_uf',
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'ID',
        field: 'id',
        maxWidth: 0,
        hide: true
      }
    ] as ColDef[]
  }
};
