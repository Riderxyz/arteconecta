import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { MessageInterface } from "../models/notification.interface";
import { config } from "./config";
import { map } from "rxjs/operators";

@Injectable()
export class PushNotificationService {
  private _tokenForTest = "";

  constructor(
    private http: HttpClient,
    private afMessaging: AngularFireMessaging
  ) {
    this.afMessaging.requestPermission.subscribe(res => {
     // console.log("resultado do pedido: ", res);
    });
    this.afMessaging.requestToken.subscribe(token => {
     // console.log(token);
      this.tokenForTest = token;
    });
  }

  public get tokenForTest(): string {
    return this._tokenForTest;
  }

  public set tokenForTest(tokenForTest: string) {
    this._tokenForTest = tokenForTest;
  }

  init() {
    console.log("Esta função ativa o serviço de Push");
  }
  sendNotification(item: MessageInterface) {
    return this.http.post(config.url.pushNotificationAPI, item);
  }
}
