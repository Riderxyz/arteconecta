import { Injectable } from "@angular/core";
import { config } from "./config";
import { ArteConectaInterface } from "../models/modeloNovo.interface";

import { AtividadeInterface } from "../models/atividade.interface";
import { MaterialInterface } from "../models/material.interface";
import { ObjetivoInterface } from "../models/objetivo.interface";
import { UsuarioInteface } from "../models/usuario.interface";

import * as data from "./../database/CadastroSite.json";
import * as listaMateriaisAntigos from "./../database/Materiais.json";
import * as listaObjetivosAntigos from "./../database/Objetivos.json";
import * as listaAtividadesAntigas from "./../database/Atividades.json";
import { UsuariosAntigo } from "../models/usuarioAntigo.interface";
import { DataService } from "./data.service";
import { ToastService } from "./toast.service";
import uuid from "uuid-random";
import { MateriaisAntigo } from "../models/modelosAntigos.interface";
import * as lodash from "lodash";
@Injectable()
export class CargaService {
  dadosUsuarios: UsuariosAntigo[];
  materialArr: MaterialInterface[] = [];
  objetivoArr: ObjetivoInterface[] = [];
  constructor(private dataSrv: DataService, private toastSrv: ToastService) {
    //  console.log(uuid());
    this.dadosUsuarios = (data as any).default as UsuariosAntigo[];
    this.dataSrv.getMaterialList.subscribe((materialArr) => {
      this.materialArr = materialArr;
    });
    this.dataSrv.getObjetivoList.subscribe((objetivoArr) => {
      this.objetivoArr = objetivoArr;
    });
    //  console.log(this.dadosUsuarios);
  }

  async processarCargas() {
    try {
    //  this.dataSrv.deleteFolderFromDB(config.url.base);
     // await this.carregarUsuario();
     // alert("Carreguei Usuários => Faltam Materiais, Objetivos e Atividades");
    //  await this.carregarMateriais();
      alert("Carreguei Usuários e Materiais => Faltam Objetivos e Atividades");
      await this.carregarObjetivos();
      alert("Carreguei Usuários e Materiais e Objetivos  => Falta Atividades");
    //  await this.carregarAtividades();
      alert("Done");
    } catch (error) {
      alert(error)
    }
  }
  carregarUsuario() {
    const x: UsuarioInteface[] = [] as UsuarioInteface[];
    const r: ArteConectaInterface = {} as ArteConectaInterface;
    var cont = 0;
    this.dadosUsuarios.forEach((element) => {
      if (element.Excluido === "0") {
        const u: UsuarioInteface = {} as UsuarioInteface;
        /*         console.log("ELEMENT ==> ", element);
         */
        u.id = uuid();//cont.toString();// uuid();
        u.nome =
          element.NomeCompleto === undefined || element.Apelido === ""
            ? " "
            : element.NomeCompleto;
        u.observacoes =
          element.Observacoes === undefined || element.Apelido === ""
            ? " "
            : element.Observacoes;
        u.profissao =
          element.Profissao === undefined || element.Apelido === ""
            ? (" " as any)
            : (element.Profissao as any);
        u.sexo =
          element.Sexo === undefined || element.Apelido === ""
            ? " "
            : element.Sexo;
        u.emailPrincipal =
          element.Email === undefined || element.Apelido === ""
            ? " "
            : element.Email;
        u.endereco_cidade =
          element.Cidade === undefined || element.Apelido === ""
            ? " "
            : element.Cidade;
        u.endereco_uf =
          element.Estado === undefined || element.Apelido === ""
            ? " "
            : element.Estado;
        u.endereco_pais =
          element.Pais === undefined || element.Apelido === ""
            ? " "
            : element.Pais;
        u.filhos =
          element.Filhos === undefined || element.Apelido === ""
            ? " "
            : element.Filhos;
        u.observacoes =
          element.Observacoes === undefined || element.Apelido === ""
            ? " "
            : element.Observacoes;
        u.updateBy =
          element.AtualizadoPor === undefined || element.Apelido === ""
            ? " "
            : element.AtualizadoPor;
        u.updateDate = new Date(element.DataHoraAtualizacao);
        x.push(u);
        this.save(u.id, u, config.url.dbUser);
        cont++;
      }
    });
    r.usuarios = x;
  }

  carregarMateriais() {
    console.log("Iniciando Carga de Materiais");
    const x: MaterialInterface[] = [] as MaterialInterface[];
    const tempArray: string[] = [];
    var cont = 0;
    const listaMateriaisold: MateriaisAntigo[] = (listaMateriaisAntigos as any)
      .default as MateriaisAntigo[];
    listaMateriaisold.forEach((element) => {
      if (tempArray.indexOf(element.Material) == -1) {
        let u: MaterialInterface = {} as MaterialInterface;
        u.material = this.capitalizeFirstLetter(element.Material);
        u.id = uuid();// cont.toString();// uuid(); //config.getRandomId();
        x.push(u);
        this.save(u.id, u, config.url.dbMateriais);
        tempArray.push(element.Material);
        cont++;
      } else {
        console.log(" já achei o " + element.Material.toString());
      }
      //
    });

    //  console.log("novos materiais ", x);
  }

  carregarObjetivos() {
    const x: ObjetivoInterface[] = [] as ObjetivoInterface[];
    const lista = (listaObjetivosAntigos as any).default;
    var cont = 0;
    try {
      lista.forEach((element) => {
        let u: ObjetivoInterface = {} as ObjetivoInterface;
        u.objetivo = this.capitalizeFirstLetter(element.Objetivo);
        u.id = uuid(); //cont.toString();//  // element.id; // uuid(); //config.getRandomId();
        x.push(u);
        this.save(u.id, u, config.url.dbObjetivos);
        cont++;
      });
    } catch (error) {
      alert(error);
    }
  }

  carregarAtividades() {
    try {
      const x: AtividadeInterface[] = [] as AtividadeInterface[];
      const lista = (listaAtividadesAntigas as any).default;
      console.log((listaAtividadesAntigas as any).default.length);
      var cont = 0;
      lista.forEach((element, index) => {
        if (element.Excluido === "0") {
          const a: AtividadeInterface = {} as AtividadeInterface;
          a.id = uuid();//cont.toString(); uuid();
          /*         a.tipo =
                    element.IdTipo === undefined || element.IdTipo === ""
                      ? " "
                      : element.IdTipo; */
          if (element.Edicao !== undefined && element.Edicao !== null) {
            if (element.Edicao === "1") {
              a.tipo = "VideoAula";
            } else {
              a.tipo = "Proposta";
            }
          } else {
            a.tipo = "Proposta"
          }

          a.titulo =
            element.Titulo === undefined || element.Titulo === ""
              ? " "
              : element.Titulo;
          a.update_autor =
            element.AtualizadoPor === undefined || element.AtualizadoPor === ""
              ? " "
              : element.AtualizadoPor;
          a.update_data =
            element.DataHoraAtualizacao === undefined ||
              element.DataHoraAtualizacao === ""
              ? " "
              : element.DataHoraAtualizacao;
          a.update_observacao = " ";
          a.update_tipo = " ";
          a.videourl =
            element.URLvideoEmbed === undefined || element.URLvideoEmbed === ""
              ? " "
              : element.URLvideoEmbed;
          a.lista_atividade = null;

          a.maturidadeMin =
            element.MaturidadeMin === undefined || element.MaturidadeMin === ""
              ? " "
              : element.MaturidadeMin;
          a.maturidadeMax =
            element.MaturidadeMax === undefined || element.MaturidadeMax === ""
              ? " "
              : element.MaturidadeMax;
          // tslint:disable-next-line: max-line-length
          a.numeroParticipantes =
            element.IdParticipantes === undefined ||
              element.IdParticipantes === ""
              ? " "
              : element.IdParticipantes;
          a.descricao =
            element.Descricao === undefined || element.Descricao === ""
              ? " "
              : element.Descricao;
          a.sugestoes =
            element.Sugestões === undefined || element.Sugestões === ""
              ? " "
              : element.Sugestões;
          a.imagem =
            element.Imagem1 === undefined || element.Imagem1 === ""
              ? " "
              : element.Imagem1;
          a.legendasVideo =
            element.LegendaVideo === undefined || element.LegendaVideo === ""
              ? " "
              : element.LegendaVideo;
          a.numedicao =
            element.numEdicao === undefined || element.numEdicao === ""
              ? " "
              : element.numEdicao;

          a.criadopor =
            element.CriadoPor === undefined || element.CriadoPor === ""
              ? " "
              : element.CriadoPor;
          a.data_criacao =
            element.DataHoraCriacao === undefined ||
              element.DataHoraCriacao === ""
              ? " "
              : element.DataHoraCriacao;
          a.excluido = false;

          if (element.IndicadoPais !== undefined || element.IndicadoPais !== "") {
            if (element.IndicadoPais === "0") {
              a.indicacaopais = false;
            } else {
              a.indicacaopais = true;
            }
          }
          if (element.Edicao !== undefined || element.Edicao !== "") {
            if (element.Edicao === "0") {
              a.edicao = false;
            } else {
              a.edicao = true;
            }
          }
          if (element.Free !== undefined || element.Free !== "") {
            if (element.Free === "0") {
              a.free = false;
            } else {
              a.free = true;
            }
          }
          if (element.Publicado !== undefined || element.Publicado !== "") {
            if (element.Publicado === "0") {
              a.publicado = false;
            } else {
              a.publicado = true;
            }
          }
          if (element.Revisado !== undefined || element.Revisado !== "") {
            if (element.Revisado === "0") {
              a.revisao = false;
            } else {
              a.revisao = true;
            }
          }

          a.lista_material =
            element.Materiais === undefined || element.Materiais === ""
              ? " "
              : element.Materiais;
          a.lista_objetivo =
            element.Objetivos === undefined || element.Objetivos === ""
              ? " "
              : element.Objetivos;
          let transforMat: any = a.lista_material;
          let transformObj: any = a.lista_objetivo;
          let resultMatArr = [];
          let resultObj = [];
          if (transforMat !== null) {
            transforMat = transforMat.split(/\s*,\s*/);
            transforMat.forEach((l, index) => {
              if (this.getMaterialId(l)) {
                resultMatArr.push(this.getMaterialId(l));
              }
              if (index === transforMat.length - 1) {
                a.lista_material = resultMatArr;
              }
            });
          }
          if (transformObj !== null) {
            transformObj = transformObj.split(/\s*,\s*/);
            transformObj.forEach((l, index) => {
              if (this.getObjetivoId(l)) {
                resultObj.push(this.getObjetivoId(l));
              }
              if (index === transformObj.length - 1) {
                a.lista_objetivo = resultObj;
              }
            });
          }
          x.push(a);
          this.save(a.id, a, config.url.dbAtividades);
          cont++;
        }
      });
    } catch (error) {
      console.log(error)
    }

  }

  private save(id, valueToSave: any, doc: string) {
    valueToSave.id = id;
    //this.dataSrv.saveTo(valueToSave, doc);
    this.dataSrv.saveToFireStore(valueToSave, doc);
  }

  private getMaterialId(str: string) {
    let retorno = "";
    this.materialArr.forEach((t) => {
      if (t.material.toLowerCase() === str.toLowerCase()) {
        retorno = t.id;
      }
    });
    return retorno;
  }

  private getObjetivoId(str: string) {
    let retorno = "";
    this.objetivoArr.forEach((t) => {
      if (t.objetivo.toLowerCase() === str.toLowerCase()) {
        retorno = t.id;
      }
    });
    return retorno;
  }
  private capitalizeFirstLetter(str): string {
    // converting first letter to uppercase
    const capitalized = str != null ? str.replace(/^./, str[0].toUpperCase()) : str;
    return capitalized;
  }
}
