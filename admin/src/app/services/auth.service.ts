import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from "firebase/app";
import { LoginObjInterface } from "../models/loginObj.interface";
import { Router } from "@angular/router";
import { ToastService } from "./toast.service";

@Injectable()
export class AuthService {
  constructor(
    public afAuth: AngularFireAuth,
    private router: Router,
    private toastSrv: ToastService
  ) {}

  async logInWithEmail(userObj: LoginObjInterface, isUserSaveable: boolean) {
    /* const trace = perf.trace("userLogin"); */
    /* trace.start(); */
    const path = firebase.default.auth.Auth.Persistence.LOCAL;
    this.afAuth.setPersistence(path)
      .then(async () => {
        try {
          const loginNormal = await this.afAuth.signInWithEmailAndPassword(
            userObj.email,
            userObj.password
          );
         /*  trace.putAttribute("verified", `${loginNormal.user.emailVerified}`);
          trace.stop(); */
          this.router.navigateByUrl("/dashboard");
        } catch (error) {
          this.onLoginError();
         /*  trace.putAttribute("errorCode", error.code);
          trace.stop(); */
        }
      })
      .catch(error => {
        console.error("Erro de Login", error);
       /*  trace.putAttribute("errorCode", error.code);
        trace.stop(); */
      });
  }

  get currentUserObservable() {
    return this.afAuth.authState;
  }

  onLoginSucess() {
    this.toastSrv.showToastSucess("Seu login foi efetuado com sucesso");
  }
  onLoginError() {
    this.toastSrv.showToastError(
      "Seu login não pode ser feito",
      "Tente novamente"
    );
  }

  async LogOut() {
    await this.afAuth.signOut();
  }
}
