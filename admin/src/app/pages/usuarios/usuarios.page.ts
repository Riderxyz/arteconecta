import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { DataService } from '../../services/data.service';
import { CentralRxJsService } from '../../services/centralRXJS.service';
import { ToastService } from '../../services/toast.service';
import { ColDef } from 'ag-grid-community';
import { config } from '../../services/config';
import Swal, { SweetAlertOptions } from 'sweetalert2';
import { fadeInUpOnEnterAnimation } from 'angular-animations';
import { UsuarioInteface } from '../../models/usuario.interface';
import { UsuarioModal } from '../../components/modals/usuario-modal/usuario-modal.component';

@Component({
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
  animations: [
    fadeInUpOnEnterAnimation({ anchor: 'usuarioEnter', translate: '100%' })
  ]
})
export class UsuariosPage implements OnInit {
  fullEnvironment = environment;
  usuarioArr: UsuarioInteface[] = [];
  selectedRows: UsuarioInteface[] = [];
  filtro = '';
  allowEdit = false;
  constructor(
    public dialog: MatDialog,
    private dataSrv: DataService,
    private centralRXJS: CentralRxJsService,
    private toastSrv: ToastService
  ) { }

  ngOnInit() {
    this.dataSrv.getUsuariosList
      .subscribe((res) => {
        this.usuarioArr = res;
      });

  }
  get createColumnDefs(): Array<ColDef> {
    return [
      {
        headerName: 'nome'.toLocaleUpperCase(),
        field: 'nome',
        width: 30,
        checkboxSelection: ((params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }),
        headerCheckboxSelection: ((params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        })
      },
      {
        headerName: 'email'.toLocaleUpperCase(),
        field: 'emailPrincipal',
        width: 15,
        autoHeight: true,
      },
      {
        headerName: 'celular'.toLocaleUpperCase(),
        field: 'celular',
        width: 15,
        autoHeight: true,
      },
      {
        headerName: 'cidade'.toLocaleUpperCase(),
        field: 'endereco_cidade',
        width: 15,
        autoHeight: true,
      },
      {
        headerName: 'uf'.toLocaleUpperCase(),
        field: 'endereco_uf',
        width: 15,
        autoHeight: true,
      },
      {
        headerName: 'ID',
        field: 'id',
        maxWidth: 0,
        hide: true
      },
    ];
  }
  get colunasFiltraveis() {
    return [
      'nome',
      'emailPrincipal',
      'telefone',
      'endereco_cidade',
      'endereco_uf',
    ];
  }

  onGridSelectedRow(event) {
    this.selectedRows = event;
    if (this.selectedRows.length === 1) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
  }

  onGridDobleClick(event) {
    this.editItem(event);
  }

  addNewItem(): void {
    const dialogRef = this.dialog.open(UsuarioModal, {
      minWidth: '250px',
      data: null
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  editItem(rowData?) {
    if (rowData === undefined) {
      rowData = this.selectedRows[0];
    }
    const dialogRef = this.dialog.open(UsuarioModal, {
      minWidth: '250px',
      data: rowData
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  deleteItem() {
    const toastConfig: SweetAlertOptions = {
      title: 'Tem certeza',
      text: 'Não será possivel reverter esta ação',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, delete-os!'
    };
    this.toastSrv.showConfirmDialog(toastConfig).then((result) => {
      if (result.value) {
        const deleteOBj = {
          path: config.url.dbUser,
          arr: this.selectedRows
        };
        this.dataSrv.deleteFromDB = deleteOBj;
        Swal.fire(
          'Deletados',
          'Arquivos selecionados foram deletados',
          'success'
        );
      }
    });
  }
}
