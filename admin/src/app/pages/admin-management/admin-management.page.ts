import { AngularFireAuth } from "@angular/fire/auth";
import { fadeInRightOnEnterAnimation } from "angular-animations";
import { ColDef } from "ag-grid-community";
import { config } from "src/app/services/config";
import { ToastService } from "src/app/services/toast.service";
import { DataService } from "src/app/services/data.service";
import { environment } from "src/environments/environment";
import { AdminUserModal } from "./../../components/modals/admin-user-modal/admin-user-modal.component";
import { MatDialog } from "@angular/material/dialog";
import { Component, OnInit } from "@angular/core";
import { AdminUsuarioInteface } from "src/app/models/adminUsuario.interface";
import Swal, { SweetAlertOptions } from "sweetalert2";

@Component({
  templateUrl: "./admin-management.page.html",
  styleUrls: ["./admin-management.page.scss"],
  animations: [
    fadeInRightOnEnterAnimation({
      anchor: "adminUsuarioEnter",
      translate: "100%",
    }),
  ],
})
export class AdminManagementPage implements OnInit {
  fullEnvironment = environment;
  AdminUsuarioArr: AdminUsuarioInteface[] = [];
  selectedRows: AdminUsuarioInteface[] = [];
  filtro = "";
  allowEdit = false;
  constructor(
    public dialog: MatDialog,
    private dataSrv: DataService,
    private toastSrv: ToastService,
    private afAuth: AngularFireAuth
  ) {}

  ngOnInit(): void {
    this.dataSrv.getAdministradores.subscribe((res) => {
      this.AdminUsuarioArr = res;
    });
  }
  get createColumnDefs(): Array<ColDef> {
    return [
      {
        headerName: "nome".toLocaleUpperCase(),
        field: "nome",
        width: 30,
        checkboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
      },
      {
        headerName: "email".toLocaleUpperCase(),
        field: "emailPrincipal",
        width: 15,
        autoHeight: true,
      },
      {
        headerName: "celular".toLocaleUpperCase(),
        field: "celular",
        width: 15,
        autoHeight: true,
      },
      {
        headerName: "usuario ativo".toLocaleUpperCase(),
        field: "ativo",
        width: 15,
        autoHeight: true,
      },
      {
        headerName: "Data de cadastro".toLocaleUpperCase(),
        field: "dt_cadastro",
        width: 15,
        autoHeight: true,
      },
      {
        headerName: "ID",
        field: "id",
        maxWidth: 0,
        hide: true,
      },
    ];
  }

  get colunasFiltraveis() {
    return ["nome", "emailPrincipal", "celular"];
  }
  startFilter(ev) {}

  onGridSelectedRow(event) {
    this.selectedRows = event;
    if (this.selectedRows.length === 1) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
  }
  onGridDobleClick(event) {
    this.editItem(event);
  }
  addNewItem() {
    const dialogRef = this.dialog.open(AdminUserModal, {
      minWidth: "250px",
      data: null,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
  editItem(rowData?) {
    if (rowData === undefined) {
      rowData = this.selectedRows[0];
    }
    const dialogRef = this.dialog.open(AdminUserModal, {
      minWidth: "250px",
      data: rowData,
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
  deleteItem() {
    const toastConfig: SweetAlertOptions = {
      title: "Tem certeza",
      text: "Não será possivel reverter esta ação",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim, delete-os!",
    };
    this.toastSrv.showConfirmDialog(toastConfig).then((result) => {
      if (result.value) {
        const deleteOBj = {
          path: config.url.dbAdmins,
          arr: this.selectedRows,
        };
        this.dataSrv.deleteFromDB = deleteOBj;
        Swal.fire(
          "Deletados",
          "Arquivos selecionados foram deletados",
          "success"
        );
      }
    });
  }
  mudarSenha(rowData?) {
    if (rowData === undefined) {
      rowData = this.selectedRows[0];
      this.afAuth
        .sendPasswordResetEmail(rowData.emailPrincipal)
        .then(() => {
          this.toastSrv.showToastSucess(
            "Link enviado!",
            "O Administrador " +
              rowData.nome +
              " recebeu em seu email um link para trocar a sua senha"
          );
        })
        .catch((err) => {
          this.toastSrv.showToastError(
            "Não foi possivel enviar o link para o Administrador",
            "Tente novamente mais tarde"
          );
        });
    }
  }
}
