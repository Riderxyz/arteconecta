import { Component, OnInit } from "@angular/core";
import { DataService } from "../../services/data.service";
import { ColDef } from "ag-grid-community";
import { MatDialog } from "@angular/material/dialog";
import Swal, { SweetAlertOptions } from "sweetalert2";
import { ToastService } from "../../services/toast.service";
import { environment } from "src/environments/environment";
import { CentralRxJsService } from "../../services/centralRXJS.service";
import { config } from "../../services/config";
import { fadeInRightOnEnterAnimation } from "angular-animations";

import { AtividadeInterface } from "../../models/atividade.interface";
import { MaterialInterface } from "../../models/material.interface";
import { ObjetivoInterface } from "../../models/objetivo.interface";

import { AtividadeModal } from "src/app/components/modals/atividade-modal/atividade-modal.component";
import moment from "moment";
import lodash from "lodash";
import { AngularFireDatabase } from "@angular/fire/database";

@Component({
  templateUrl:"./atividades.page.html",
  styleUrls: ["./atividades.page.scss"],
  animations: [
    fadeInRightOnEnterAnimation({
      anchor: "atividadeEnter",
      translate: "100%",
    }),
  ],
})
export class AtividadesPage implements OnInit {
  fullEnvironment = environment;
  atividadesArr: AtividadeInterface[] = [];
  selectedRows: AtividadeInterface[] = [];

  justForTestMaterial: MaterialInterface[] = [];
  justForTestObjetivo: ObjetivoInterface[] = [];
  filtro = "";
  allowEdit = false;

  constructor(
    public dialog: MatDialog,
    private dataSrv: DataService,
    private centralRXJS: CentralRxJsService,
    private toastSrv: ToastService,
    public db: AngularFireDatabase
  ) {}

  ngOnInit(): void {
    this.dataSrv.getMaterialList.subscribe((res) => {
      this.justForTestMaterial = res;
    });
    this.dataSrv.getObjetivoList.subscribe((res) => {
      this.justForTestObjetivo = res;
    });
    this.dataSrv.getAtividadeList.subscribe((res) => {
      this.atividadesArr = res;
      this.atividadesArr.forEach(element => {
        if(element.tipo !== '1') {
          console.log(element);

        }
      });
    });

    console.clear();
/*     this.db
    .list(config.url.base + config.url.dbAtividades).valueChanges()
    .subscribe((res:AtividadeInterface[]) => {
      res.forEach(element => {
        if(element.tipo === '1') {
          console.log(element);

        }
      });
      // .set(saveObj.id, saveObj)
      // console.log("Subiu", saveObj.id);
    }); */
  }

  transformToString() {
    this.atividadesArr.forEach((atividadesRes: any) => {
      if (typeof atividadesRes.lista_atividade === "string") {
        /*        lodash.find(this.justForTestMaterial, ((r) => {
                 return r.material === atividadesRes.lista_atividade;
               })) */
      }
      if (typeof atividadesRes.lista_material === "string") {
      }
      if (typeof atividadesRes.lista_objetivo === "string") {
        const newArray: string[] = atividadesRes.lista_objetivo.split(",");
        const newArrayofId: any[] = [];
        newArray.forEach((element) => {
          const foundItem = lodash.find(this.justForTestObjetivo, (r) => {
            return r.objetivo.trim() === element.trim();
          });

          if (foundItem) {
            newArrayofId.push(foundItem.id);
          }
        });
        atividadesRes.lista_objetivo = newArrayofId;
        const saveObj: AtividadeInterface = atividadesRes;
      }
    });
  }

  get createColumnDefs(): Array<ColDef> {
    return config.columnDef.atividadeGrid;
  }
  get colunasFiltraveis() {
    return ["titulo", "criadopor", "sugestaoComentario", "comentario", "data"];
  }
  startFilter(ev) {
    this.centralRXJS.sendData = {
      key: config.rxjsCentralKeys.onGridFilter,
      data: ev,
    };
    // this.gridApi.onFilterChanged();
  }
  onGridSelectedRow(event) {
    this.selectedRows = event;
    if (this.selectedRows.length === 1) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
  }
  onGridDobleClick(event) {
    this.editItem(event);
  }
  addNewItem(data?): void {
    const dialogRef = this.dialog.open(AtividadeModal, {
      minWidth: "250px",
      data: data ? data : null,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
  editItem(rowData?) {
    if (rowData === undefined) {
      rowData = this.selectedRows[0];
    }
    const dialogRef = this.dialog.open(AtividadeModal, {
      minWidth: "250px",
      data: rowData,
    });
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }

  deleteItem() {
    const toastConfig: SweetAlertOptions = {
      title: "Tem certeza",
      text: "Não será possivel reverter esta ação",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim, delete-os!",
    };
    this.toastSrv.showConfirmDialog(toastConfig).then((result) => {
      if (result.value) {
        const deleteOBj = {
          path: config.url.dbAtividades,
          arr: this.selectedRows,
        };
        this.dataSrv.deleteFromDB = deleteOBj;
        Swal.fire(
          "Deletados",
          "Arquivos selecionados foram deletados",
          "success"
        );
      }
    });
  }
}
