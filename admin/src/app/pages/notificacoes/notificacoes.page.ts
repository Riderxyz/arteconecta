import { DataService } from "src/app/services/data.service";
import { Component, OnInit } from "@angular/core";
import { MessageInterface } from "../../models/notification.interface";
import { PushNotificationService } from "src/app/services/notification.service";
import { UsuarioInteface } from "src/app/models/usuario.interface";
@Component({
  templateUrl: "./notificacoes.page.html",
  styleUrls: ["./notificacoes.page.scss"],
})
export class NotificacoesPage implements OnInit {
  notification: MessageInterface = {
    pushMessage: {
      title: "",
      body: "",
      icon: "",
    },
    token: [],
  };
  sizetitleTxt = 0;
  titleTxt = "Envio de notificações";
  userList: UsuarioInteface[] = [];
  selectedUser = null as string;
  sendToWho: "para todos" | "especifico" | "teste" = "para todos";
  constructor(
    private pushSrv: PushNotificationService,
    private dataSrv: DataService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.startTypingTitle();
    }, 100);
    this.dataSrv.getUsuariosList.subscribe((resUsuario) => {
      let retorno = [];
      resUsuario.forEach((element) => {
        if (element.notificationTokenList !== undefined) {
          retorno.push(element);
        }
      });
      this.userList = retorno;
    });
  }

  startTypingTitle() {
    if (this.sizetitleTxt < this.titleTxt.length) {
      document.getElementById(
        "card-title-notification"
      ).innerHTML += this.titleTxt.charAt(this.sizetitleTxt);
      this.sizetitleTxt++;
      setTimeout(() => {
        this.startTypingTitle();
      }, 100);
    }
  }

  onUserSelection(ev: string) {
    if (ev.length > 0) {
      this.selectedUser = ev;
    }
  }
  sendNotification() {
    this.notification.token = [];
    switch (this.sendToWho) {
      case "especifico":
        this.dataSrv
          .getSpecificUser(this.selectedUser)
          .subscribe((usuarioObj) => {
            this.notification.token = usuarioObj.notificationTokenList;
            // this.selectedUser = usuarioObj;
          });
          this.pushSrv.sendNotification(this.notification).subscribe((res) => {
            // console.log(res);
          });
        break;
      case "para todos":
        this.userList.forEach((usuarioOBJ) => {
          if (usuarioOBJ.notificationTokenList !== undefined) {
            usuarioOBJ.notificationTokenList.forEach((element) => {
              this.notification.token.push(element);
            });
          }
        });
        this.pushSrv.sendNotification(this.notification).subscribe((res) => {
          // console.log(res);
        });
        break;
      case "teste":
        this.notification.token.push(this.pushSrv.tokenForTest);
        this.pushSrv.sendNotification(this.notification).subscribe((res) => {
          // console.log(res);
        });
        break;
    }
  }
}
