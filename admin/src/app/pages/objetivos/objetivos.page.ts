import * as lodash from "lodash";
import { Component, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";
import { MatDialog } from "@angular/material/dialog";
import { DataService } from "../../services/data.service";
import { ToastService } from "../../services/toast.service";
import { ColDef } from "ag-grid-community";
import { config } from "../../services/config";
import Swal, { SweetAlertOptions } from "sweetalert2";
import { fadeInUpOnEnterAnimation } from "angular-animations";
import { ObjetivoInterface } from "../../models/objetivo.interface";
import { ObjetivoModal } from 'src/app/components/modals/objetivo-modal/objetivo-modal.component';
import { AtividadeInterface } from "src/app/models/atividade.interface";
@Component({
  templateUrl: "./objetivos.page.html",
  styleUrls: ["./objetivos.page.scss"],
  animations: [
    fadeInUpOnEnterAnimation({ anchor: "objetivoEnter", translate: "100%" })
  ]
})
export class ObjetivosPage implements OnInit {
  fullEnvironment = environment;
  objetivoArr: ObjetivoInterface[] = [];
  atividadesArr: AtividadeInterface[] = [];
  selectedRows: ObjetivoInterface[] = [];
  filtro = "";
  allowEdit = false;
  constructor(
    public dialog: MatDialog,
    private dataSrv: DataService,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    this.dataSrv.getObjetivoList
    .subscribe((res) => {
      this.objetivoArr = res;
    });
  }

  get createColumnDefs(): Array<ColDef> {
    return [
      {
        headerName: "objetivo".toLocaleUpperCase(),
        field: "objetivo",
        width: 30,
        autoHeight: true,
        checkboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      }
    ];
  }
  onGridSelectedRow(event) {
    this.selectedRows = event;
    if (this.selectedRows.length === 1) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
  }
  onGridDobleClick(event) {
    this.editItem(event);
  }

  addNewItem(): void {
    const dialogRef = this.dialog.open(ObjetivoModal, {
        minWidth: '250px',
        data: null
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
  }
  editItem(rowData?) {
     if (rowData === undefined) {
        rowData = this.selectedRows[0];
      }
      const dialogRef = this.dialog.open(ObjetivoModal, {
        minWidth: '250px',
        data: rowData
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
  }
  deleteItem() {
    const toastConfig: SweetAlertOptions = {
      title: "Tem certeza",
      text: "Não será possivel reverter esta ação",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim, delete-os!"
    };
    this.toastSrv.showConfirmDialog(toastConfig).then(result => {
      if (result.value) {
        Swal.fire(
          "Deletados",
          "Arquivos selecionados foram deletados",
          "success"
        );
      }
    });
  }
}
