import * as lodash from "lodash";
import { Component, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";
import { MatDialog } from "@angular/material/dialog";
import { DataService } from "../../services/data.service";
import { ToastService } from "../../services/toast.service";
import { ColDef } from "ag-grid-community";
import { config } from "../../services/config";
import Swal, { SweetAlertOptions } from "sweetalert2";
import { fadeInUpOnEnterAnimation } from "angular-animations";
import { MaterialInterface } from "../../models/material.interface";
import { MaterialModal } from "src/app/components/modals/material-modal/material-modal.component";
import { AtividadeInterface } from "src/app/models/atividade.interface";

@Component({
  templateUrl: "./materiais.page.html",
  styleUrls: ["./materiais.page.scss"],
  animations: [
    fadeInUpOnEnterAnimation({ anchor: "materialEnter", translate: "100%" }),
  ],
})
export class MateriaisPage implements OnInit {
  fullEnvironment = environment;
  materialArr: MaterialInterface[] = [];
  atividadesArr: AtividadeInterface[] = [];
  selectedRows: MaterialInterface[] = [];
  filtro = "";
  allowEdit = false;
  constructor(
    public dialog: MatDialog,
    private dataSrv: DataService,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    this.dataSrv.getMaterialList.subscribe((res) => {
      this.materialArr = res;
    });
    this.dataSrv.getAtividadeList.subscribe((res) => {
      this.atividadesArr = res;
    });
  }
  get createColumnDefs(): Array<ColDef> {
    return [
      {
        headerName: "material".toLocaleUpperCase(),
        field: "material",
        width: 30,
        autoHeight: true,
        checkboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
      },
    ];
  }
  onGridSelectedRow(event) {
    this.selectedRows = event;
    if (this.selectedRows.length === 1) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
  }
  onGridDobleClick(event) {
    this.editItem(event);
  }
  addNewItem(): void {
    const dialogRef = this.dialog.open(MaterialModal, {
      minWidth: "250px",
      data: null,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
  editItem(rowData?) {
    if (rowData === undefined) {
      rowData = this.selectedRows[0];
    }
    const dialogRef = this.dialog.open(MaterialModal, {
      minWidth: "250px",
      data: rowData,
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
  deleteItem() {
    const toastConfig: SweetAlertOptions = {
      title: "Tem certeza",
      text: "Não será possivel reverter esta ação",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim, delete-os!",
    };
    this.toastSrv.showConfirmDialog(toastConfig).then(async (result) => {
      if (result.value) {
        const deleteOBj = {
          path: config.url.dbMateriais,
          arr: this.selectedRows,
        };
        this.selectedRows.forEach((ev) => {
          const resultAtividade = lodash.find(this.atividadesArr, (res) => {
            let retorno = false; // res.lista_material.toString().indexOf(ev.id);
            if (
              res.lista_material !== undefined &&
              res.lista_material.length > 0
            ) {
              const listToString = res.lista_material.toString();
              retorno = listToString.includes(ev.id);
            }
            return retorno;
            //"De duas para três dimensões"
          });
          resultAtividade.lista_material.splice(
            resultAtividade.lista_material.indexOf(ev.id),
            1
          );
          //  debugger;
          this.dataSrv.saveTo(resultAtividade, config.url.dbAtividades);
        });
        this.dataSrv.deleteFromDB = deleteOBj;

        Swal.fire(
          "Deletados",
          "Arquivos selecionados foram deletados",
          "success"
        );
      }
    });
  }
}
