import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from "@angular/core";
import { LoginObjInterface } from "../../models/loginObj.interface";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { NgForm } from "@angular/forms";
import { ToastService } from "src/app/services/toast.service";
import { CargaService } from 'src/app/services/carga.service';
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit, OnDestroy {
  sizetitleTxt = 0;
  titleTxt = "Login";
  isSaveable = false;
  @ViewChild("loginTitle", { static: true }) loginTitle: ElementRef;
  userObj: LoginObjInterface = {
    email: null,
    password: null
  };
  fullEnvironment = environment;

  constructor(
    private AuthSrv: AuthService,
    private router: Router,
    private toastSrv: ToastService,
    public cargaSrv: CargaService,
  ) {
    this.AuthSrv.currentUserObservable.subscribe(res => {
      // console.log(res);
      if (res !== null) {
        this.router.navigateByUrl("/dashboard");
      }
    });
  }

  ngOnInit() {
    this.startTypingTitle();
  }

  startTypingTitle() {
    // console.log(this.loginTitle.nativeElement.innerHTML);
    /* console.log(this.titleTxt.length); */
    if (this.sizetitleTxt < this.titleTxt.length) {
      this.loginTitle.nativeElement.innerHTML += this.titleTxt.charAt(
        this.sizetitleTxt
      );
      this.sizetitleTxt++;
      setTimeout(() => {
        this.startTypingTitle();
      }, 100);
    }
  }

  test() {
    /* this.AuthSrv.onLoginSucess(); */
    this.router.navigateByUrl("/dashboard");
  }
  login(loginForm: NgForm) {
    /* console.log("Ativando o form", loginForm); */

    if (loginForm.valid) {
      this.AuthSrv.logInWithEmail(this.userObj, this.isSaveable);
    } else {
      this.toastSrv.showToastError(
        "Não foi possivel efetuar o seu login",
        "verifique se todos os campos estão preenchidos"
      );
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // this.screenTrace.stop();
  }
}
