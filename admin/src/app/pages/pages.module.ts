import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { MaterialModule } from "./../material.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ComponentsModule } from "../components/components.module";

import { HttpClientModule } from "@angular/common/http";
import { DashboardPage } from "./dashboard/dashboard.page";
import { LoginPage } from "./login/login.page";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { UsuariosPage } from "./usuarios/usuarios.page";
import { AtividadesPage } from "./atividades/atividades.page";
import { NotificacoesPage } from "./notificacoes/notificacoes.page";
import { MateriaisPage } from "./materiais/materiais.page";
import { ObjetivosPage } from "./objetivos/objetivos.page";
import { AdminManagementPage } from "./admin-management/admin-management.page";

const FormModules = [ReactiveFormsModule, FormsModule];
@NgModule({
  imports: [
    CommonModule,
    MDBBootstrapModule.forRoot(),
    ComponentsModule,
    HttpClientModule,
    MaterialModule,
    SelectAutocompleteModule,
    ...FormModules,
  ],
  declarations: [
    DashboardPage,
    LoginPage,
    UsuariosPage,
    AtividadesPage,
    NotificacoesPage,
    MateriaisPage,
    ObjetivosPage,
    AdminManagementPage,
  ],
  exports: [
    DashboardPage,
    LoginPage,
    UsuariosPage,
    AtividadesPage,
    NotificacoesPage,
  ],
  providers: [],
})
export class PagesModule {}
