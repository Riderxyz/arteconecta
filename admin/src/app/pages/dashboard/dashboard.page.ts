import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../services/data.service';
import { StatusCardInterface } from '../../models/statusCard.interface';
import { ColDef } from 'ag-grid-community';
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../environments/environment';

import { fadeInRightOnEnterAnimation } from 'angular-animations';
import { CargaService } from 'src/app/services/carga.service';


import { AtividadeInterface } from "../../models/atividade.interface";
import { MaterialInterface } from "../../models/material.interface";
import { ObjetivoInterface } from "../../models/objetivo.interface";
import { UsuarioInteface } from "../../models/usuario.interface";


import { Router } from '@angular/router';
import moment from 'moment';
import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/toast.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
  animations: [
    fadeInRightOnEnterAnimation({ anchor: 'dashboardEnter', translate: '100%' })
  ]
})
export class DashboardPage implements OnInit, OnDestroy {
  fullEnvironment = environment;
  gridDimensions = {
    width: '100%',
    height: '40vh'
  };
  dashboardObj = {
    atividadeArr: [] as AtividadeInterface[],
    usuarioArr: [] as UsuarioInteface[],
    materialArr: [] as MaterialInterface[],
    objetivoArr: [] as ObjetivoInterface[]
  };
  toogle = false;
  isSpining = false;
  statsCardData: StatusCardInterface[] = [];
  filtro = {
    atividade: '',
    usuario: '',
    material: '',
    objetivo: ''
  };

  enableFilter = {
    atividadeGrid: false,
    usuarioGrid: false,
    materialGrid: false,
    objetivoGrid: false
  };
  constructor(
    public dialog: MatDialog,
    private dataSrv: DataService,
    public cargaSrv: CargaService,
    private route: Router
  ) { }

  ngOnInit() {
    this.starGridRender();
    var a = new ToastService();
    Object.keys(a).forEach(key => console.log(key));
    for (var member in a) {
      if (a.hasOwnProperty(member)) {
        console.log(member);
      }
    }

  }
  starGridRender() {
    this.dataSrv.getAtividadeList.subscribe(res => {
      this.dashboardObj.atividadeArr = res;
      this.startCardRender();
    });
    this.dataSrv.getUsuariosList.subscribe(res => {
      this.dashboardObj.usuarioArr = res;
      this.startCardRender();
    });

    this.dataSrv.getMaterialList.subscribe(res => {
      this.dashboardObj.materialArr = res;
    //  console.log(this.dashboardObj.materialArr[0]);
      this.startCardRender();
    });
    this.dataSrv.getObjetivoList.subscribe(res => {
      this.dashboardObj.objetivoArr = res;
   //   console.log(this.dashboardObj.objetivoArr[0]);
      this.startCardRender();
    });
  }

  startCardRender() {
    this.statsCardData = [
      {
        icone: 'fas fa-file-alt',
        titulo: 'Quantidade de atividades',
        valor: this.dashboardObj.atividadeArr.length,
        cor: 'card-header-primary'
      },
      {
        icone: 'fas fa-users',
        titulo: 'Usuarios Cadastrados',
        valor: this.dashboardObj.usuarioArr.length,
        cor: 'card-header-info'
      },
      {
        icone: 'fas fa-box',
        titulo: 'Materiais Cadastrados',
        valor: this.dashboardObj.materialArr.length,
        cor: 'card-header-warning'
      },
      {
        icone: 'fas fa-check',
        titulo: 'Objetivos Cadastrados',
        valor: this.dashboardObj.objetivoArr.length,
        cor: 'card-header-danger'
      }
    ];
  }

  isGridReady(gridId: number) {
    switch (gridId) {
      case 1:
        this.enableFilter.atividadeGrid = true;
        break;
      case 2:
        this.enableFilter.usuarioGrid = true;
        break;
      case 3:
        this.enableFilter.materialGrid = true;
        break;
      case 4:
        this.enableFilter.objetivoGrid = true;
        break;
    }
  }

  goTo(path) {
    this.route.navigateByUrl(path);
  }

  get atividadeGridColumnDef(): Array<ColDef> {
    return [
      {
        headerName: 'titulo'.toLocaleUpperCase(),
        field: 'titulo',
        width: 30,
        checkboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      },
      {
        headerName: 'criado por'.toLocaleUpperCase(),
        field: 'criadopor',
        width: 15,
        autoHeight: true
      },
      {
        headerName: 'Criado em'.toLocaleUpperCase(),
        field: 'data_criacao',
        cellRenderer: params => {
          const dia = new Date(params.data.data_criacao).getDate();
          const mes = new Date(params.data.data_criacao).getMonth() + 1;
          const ano = new Date(params.data.data_criacao).getFullYear();
          const dataTotal = dia + '/' + mes + '/' + ano;
          const retornoComentario =
            `
          <span style='text-overflow: ellipsis'  >
          ` +
          moment(params.data.update_data).format('L') +
            `
          </span>
          `;
          return retornoComentario;
        },
        width: 5
      }
    ];
  }

  get usuarioGridColumnDef(): Array<ColDef> {
    return [
      {
        headerName: 'nome'.toLocaleUpperCase(),
        field: 'nome',
        checkboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      },
      {
        headerName: 'email'.toLocaleUpperCase(),
        field: 'emailPrincipal',
        autoHeight: true
      }
    ];
  }

  get materialGridColumnDef(): Array<ColDef> {
    return [
      {
        headerName: 'Material'.toLocaleUpperCase(),
        field: 'material',
        autoHeight: true,
        checkboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      }
    ];
  }

  get objetivoGridColumnDef(): Array<ColDef> {
    return [
      {
        headerName: 'objetivos'.toLocaleUpperCase(),
        field: 'objetivo',
        autoHeight: true,
        checkboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: params => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }
      }
    ];
  }
  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
  }
}
