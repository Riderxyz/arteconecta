import { MaterialModule } from "./../material.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FooterComponent } from "./footer/footer.component";
import { StatsCardComponent } from "./stats-card/stats-card.component";
import { GridComponent } from "./grid/grid.component";

import { SelectAutocompleteModule } from "mat-select-autocomplete";
import { QuillModule } from 'ngx-quill'
// AG-GRID
import { AgGridModule } from "ag-grid-angular";
import { HeaderComponent } from "./header/header.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { ExpandableSearchComponent } from "./expandable-search/expandable-search.component";
import { MDBBootstrapModule } from "angular-bootstrap-md";
// modals
import { UsuarioModal } from "./modals/usuario-modal/usuario-modal.component";
import { MaterialModal } from "./modals/material-modal/material-modal.component";
import { ObjetivoModal } from "./modals/objetivo-modal/objetivo-modal.component";
import { AtividadeModal } from "./modals/atividade-modal/atividade-modal.component";
import { AdminUserModal } from './modals/admin-user-modal/admin-user-modal.component';
import { MAT_DATE_LOCALE } from "@angular/material/core";

const FormModules = [ReactiveFormsModule, FormsModule];

const modals = [UsuarioModal, MaterialModal, ObjetivoModal, AtividadeModal, AdminUserModal];



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    //...Material,
    QuillModule.forRoot(),
    SelectAutocompleteModule,
    MDBBootstrapModule.forRoot(),
    // Grid
    AgGridModule.withComponents([]),
    ...FormModules,
  ],
  declarations: [
    FooterComponent,
    StatsCardComponent,
    GridComponent,
    HeaderComponent,
    SidebarComponent,
    ...modals,
    ExpandableSearchComponent,
  ],
  entryComponents: [...modals],
  exports: [
    FooterComponent,
    StatsCardComponent,
    GridComponent,
    HeaderComponent,
    SidebarComponent,
    ExpandableSearchComponent,
    CommonModule,
    ...FormModules,
    MaterialModule

  ],

})
export class ComponentsModule {}
