import { Component, OnInit, Input } from '@angular/core';
import { StatusCardInterface } from '../../models/statusCard.interface';

@Component({
    selector: 'app-stats-card',
    templateUrl: './stats-card.component.html',
    styleUrls: ['./stats-card.component.scss']
})
export class StatsCardComponent implements OnInit {
    @Input() data: StatusCardInterface;
    @Input() teste: any =  '';
    constructor() { }

    ngOnInit(): void { }


    ngOnChanges(changes): void {
      //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
      //Add '${implements OnChanges}' to the class.

    }
}
