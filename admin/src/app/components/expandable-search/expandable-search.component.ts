import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  forwardRef
} from "@angular/core";
import { config } from "../../services/config";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
@Component({
  selector: "app-expandable-search",
  templateUrl: "./expandable-search.component.html",
  styleUrls: ["./expandable-search.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ExpandableSearchComponent)
    }
  ]
})
export class ExpandableSearchComponent implements OnInit, ControlValueAccessor {
  @Input() disabled = false;
  inputModel: string = "";
  private _value: string;
  inputName = config.getRandomId();
  inputId = config.getRandomId();

  onChange: any = () => {};

  onTouched: any = () => {};

  constructor() {}

  ngOnInit(): void {}

  public get value() {
    return this._value;
  }

  public set value(v){
    this._value = v;
    this.onChange(this._value);
    this.onTouched();
  }

  writeValue(obj: any): void {
    this._value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
