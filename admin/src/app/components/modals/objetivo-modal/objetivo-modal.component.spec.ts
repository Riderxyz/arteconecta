import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetivoModal } from './objetivo-modal.component';

describe('ObjetivoModal', () => {
  let component: ObjetivoModal;
  let fixture: ComponentFixture<ObjetivoModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetivoModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetivoModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
