import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DataService } from "src/app/services/data.service";
import { config } from "../../../services/config";
import { ToastService } from "src/app/services/toast.service";

import { ObjetivoInterface } from "../../../models/objetivo.interface";


@Component({
  templateUrl: "./objetivo-modal.component.html",
  styleUrls: ["./objetivo-modal.component.scss"]
})
export class ObjetivoModal implements OnInit {
  @ViewChild("ModalTitle", { static: true }) ModalTitle: ElementRef;
  titleTxt = "";
  modalValues: ObjetivoInterface = {} as any;
  sizetitleTxt = 0;
  headerColor = "";
  constructor(
    public dialogRef: MatDialogRef<ObjetivoModal>,
    @Inject(MAT_DIALOG_DATA) public data: ObjetivoInterface,
    private dataSrv: DataService,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    if (this.data === null) {
      this.titleTxt = "ADICIONANDO";
      this.headerColor = "card-header-success";
      this.startTypingTitle();
    } else {
      this.titleTxt = "EDITANDO";
      this.modalValues = this.data;
      this.headerColor = "card-header-warning";
      this.startTypingTitle();
    }
  }
  startTypingTitle() {
    if (this.sizetitleTxt < this.titleTxt.length) {
      this.ModalTitle.nativeElement.innerHTML += this.titleTxt.charAt(
        this.sizetitleTxt
      );
      this.sizetitleTxt++;
      setTimeout(() => {
        this.startTypingTitle();
      }, 100);
    }
  }

  save(ev) {
    if (this.modalValues.id === undefined) {
      this.modalValues.id = config.getRandomId();
    }
    this.dataSrv.saveTo(this.modalValues, config.url.dbObjetivos);
    this.dialogRef.close();
  }
}
