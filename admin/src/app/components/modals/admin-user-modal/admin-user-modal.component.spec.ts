import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUserModal } from './admin-user-modal.component';

describe('AdminUserModal', () => {
  let component: AdminUserModal;
  let fixture: ComponentFixture<AdminUserModal>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminUserModal ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUserModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
