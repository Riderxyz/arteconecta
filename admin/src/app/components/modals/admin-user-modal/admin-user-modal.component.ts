import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { AdminUsuarioInteface } from "src/app/models/adminUsuario.interface";

import { DataService } from "src/app/services/data.service";
import { config } from "../../../services/config";
import { AngularFireAuth } from "@angular/fire/auth";
import { ToastService } from "src/app/services/toast.service";
import { FormControl } from "@angular/forms";

import moment from "moment";

@Component({
  templateUrl: "./admin-user-modal.component.html",
  styleUrls: ["./admin-user-modal.component.scss"],
})
export class AdminUserModal implements OnInit {
  @ViewChild("ModalTitle", { static: true })
  ModalTitle: ElementRef;
  titleTxt = "";
  modalValues: AdminUsuarioInteface = {} as any;
  sizetitleTxt = 0;
  headerColor = "";
  constructor(
    public dialogRef: MatDialogRef<AdminUsuarioInteface>,
    @Inject(MAT_DIALOG_DATA) public data: AdminUsuarioInteface,
    private dataSrv: DataService,
    private toastSrv: ToastService,
    private afAuth: AngularFireAuth
  ) {
    this.afAuth.authState.subscribe((res) => {
      // console.log(res.displayName);
    });
  }

  ngOnInit(): void {
    if (this.data === null) {
      this.titleTxt = "ADICIONANDO";
      this.headerColor = "card-header-success";
      this.startTypingTitle();
    } else {
      this.titleTxt = "EDITANDO";
      this.modalValues = this.data;
      console.clear();
      setTimeout(() => {
        //   this.fillModalValues = this.data;
      }, 100);
      this.headerColor = "card-header-warning";
      this.startTypingTitle();
    }
  }

  startTypingTitle() {
    if (this.sizetitleTxt < this.titleTxt.length) {
      this.ModalTitle.nativeElement.innerHTML += this.titleTxt.charAt(
        this.sizetitleTxt
      );
      this.sizetitleTxt++;
      setTimeout(() => {
        this.startTypingTitle();
      }, 100);
    }
  }
  mudarSenha() {
    this.afAuth.sendPasswordResetEmail(this.modalValues.emailPrincipal);
  }
  save(ev) {
    if (this.modalValues.id === undefined) {
      this.afAuth
        .createUserWithEmailAndPassword(
          this.modalValues.emailPrincipal,
          this.modalValues.senha
        )
        .then((res) => {
          this.modalValues.dt_cadastro = moment(new Date()).format("L");
          this.modalValues.updateDate = moment(new Date()).format("L");
          this.modalValues.id = res.user.uid;
          res.user.updateProfile({
            displayName: this.modalValues.nome,
          });
          this.dataSrv.saveTo(this.modalValues, config.url.dbAdmins);
          this.toastSrv.showToastSucess(
            "Administrador Incluido!",
            "O usuario " + this.modalValues.nome + " foi incluido com sucesso"
          );
        })
        .catch((err) => {
          this.toastSrv.showToastError(
            "Não foi possivel incluir o Administrador",
            "Tente novamente mais tarde"
          );
        });
    } else {
      this.modalValues.updateDate = moment(new Date()).format("L");
      this.dataSrv.saveTo(this.modalValues, config.url.dbAdmins);
    }
    this.dialogRef.close();
  }
}
