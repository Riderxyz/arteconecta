import { AuthService } from './../../../services/auth.service';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import * as lodash from "lodash";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatChipEvent } from "@angular/material/chips";

import { AtividadeInterface } from "../../../models/atividade.interface";
import { MaterialInterface } from "../../../models/material.interface";
import { ObjetivoInterface } from "../../../models/objetivo.interface";

import { DataService } from "src/app/services/data.service";
import { config } from "../../../services/config";
import { AngularFireAuth } from "@angular/fire/auth";
import { ToastService } from "src/app/services/toast.service";
import { FormControl } from "@angular/forms";
// import { SelectAutocompleteComponent } from 'mat-select-autocomplete';

@Component({
  templateUrl: "./atividade-modal.component.html",
  styleUrls: ["./atividade-modal.component.scss"],
})
export class AtividadeModal implements OnInit {
  @ViewChild("ModalTitle", { static: true }) ModalTitle: ElementRef;
  titleTxt: "ADICIONANDO" | "EDITANDO" | "" = "";
  modalValues: AtividadeInterface = {} as any;
  sizetitleTxt = 0;
  headerColor = "";

  tiposProposta = new FormControl();
  tiposPropostaLista: string[] = [
    "Plástica",
    "Corporal",
    "Poética",
    "Plástica / Corporal",
  ];

  participantesObj = new FormControl();
  participantesObjLista: string[] = [
    "Individual",
    "Dupla",
    "Grupo",
    "Individual para grupo",
    "Grupo para individual",
  ];

  public grupoMateriais: FormControl = new FormControl();
  public grupoObjetivos: FormControl = new FormControl();
  public grupoAtividades: FormControl = new FormControl();

  selectLists = {
    atividades: [] as { id: any; name: string }[],
    material: [] as { id: any; name: string }[],
    objetivo: [] as { id: any; name: string }[],
  };
  colTextAreas = {
    comentario: "p-col-6",
    sugestaoComentario: "p-col-6",
  };
  onSugestComentarioFocus = false;
  /*   @ViewChild(SelectAutocompleteComponent, { static: true })
  multiSelect: SelectAutocompleteComponent; */
  constructor(
    public dialogRef: MatDialogRef<AtividadeModal>,
    @Inject(MAT_DIALOG_DATA) public data: AtividadeInterface,
    private dataSrv: DataService,
    private toastSrv: ToastService,
    private AuthSrv: AuthService,
    private afAuth: AngularFireAuth
  ) {}

  ngOnInit(): void {
    this.modalValues.lista_atividade = [];
    if (this.data === null) {
      this.titleTxt = "ADICIONANDO";
      this.headerColor = "card-header-success";
      this.startTypingTitle();
      this.modalValues.lista_atividade = [];
      this.modalValues.lista_material = [];
      this.modalValues.lista_objetivo = [];
      this.startSelectLists();
    } else {
      this.titleTxt = "EDITANDO";
      this.headerColor = "card-header-warning";
      this.modalValues = this.data;
      this.startTypingTitle();
      this.startSelectLists();
    }
  }

  async startSelectLists() {
    /*     this.modalValues.lista_atividade = [];
        this.modalValues.lista_material = [];
        this.modalValues.lista_objetivo = []; */
    await this.dataSrv.getAtividadeList.subscribe(async (res) => {
      res.forEach((element) => {
        this.selectLists.atividades.push({
          id: element.id,
          name: element.titulo,
        });
      });
      this.selectLists.atividades = lodash.orderBy(
        this.selectLists.atividades,
        "name",
        "asc"
      );
    });
    await this.dataSrv.getMaterialList.subscribe((res) => {
      for (let index = 0; index < res.length; index++) {
        const element = res[index];
        this.selectLists.material.push({
          id: element.id,
          name: element.material,
        });
      }
      this.selectLists.material = lodash.orderBy(
        this.selectLists.material,
        "name",
        "asc"
      );
    });
    this.dataSrv.getObjetivoList.subscribe((res) => {
      res.forEach((element) => {
        this.selectLists.objetivo.push({
          id: element.id,
          name: element.objetivo,
        });
      });
      this.selectLists.objetivo = lodash.orderBy(
        this.selectLists.objetivo,
        "name",
        "asc"
      );
    });
  }
  startTypingTitle() {
    if (this.sizetitleTxt < this.titleTxt.length) {
      this.ModalTitle.nativeElement.innerHTML += this.titleTxt.charAt(
        this.sizetitleTxt
      );
      this.sizetitleTxt++;
      setTimeout(() => {
        this.startTypingTitle();
      }, 100);
    }
  }
  expandTextArea(textAreaName: string) {
    if (textAreaName === "ModalValueComentario") {
      (this.colTextAreas.comentario = "p-col-12"),
        (this.colTextAreas.sugestaoComentario = "p-col-12");
    } else {
      this.onSugestComentarioFocus = true;
      this.colTextAreas.comentario = "p-col-12";
      this.colTextAreas.sugestaoComentario = "p-col-12";
    }
  }
  closeTextArea(textAreaName: string) {
    if (textAreaName === "ModalValueComentario") {
      (this.colTextAreas.comentario = "p-col-6"),
        (this.colTextAreas.sugestaoComentario = "p-col-6");
    } else {
      this.onSugestComentarioFocus = false;
      this.colTextAreas.comentario = "p-col-6";
      this.colTextAreas.sugestaoComentario = "p-col-6";
    }
  }
  selectedAtividade(event) {
    this.modalValues.lista_atividade = event;
  }
  selectedMaterial(event) {
    this.modalValues.lista_material = event;
  }
  selectedObjetivo(event) {
    this.modalValues.lista_objetivo = event;
  }

  renderSelectedItems(selectedEvent: string, path: number) {
    let retorno = null;
    switch (path) {
      case 1:
        retorno = lodash.find(this.selectLists.atividades, (ev) => {
          return ev.id === selectedEvent;
        });
        break;
      case 2:
        retorno = lodash.find(this.selectLists.material, (ev) => {
          return ev.id === selectedEvent;
        });
        break;
      case 3:
        retorno = lodash.find(this.selectLists.objetivo, (ev) => {
          return ev.id === selectedEvent;
        });
        break;
    }
    return retorno;
  }

  deleteRenderChips(selectedEvent: string) {
    const array = this.modalValues.lista_atividade;
    const index = array.indexOf(selectedEvent);
    if (index >= 0) {
      array.splice(index, 1);
      this.modalValues.lista_atividade = array;
    }
  }

  test(ev) {
    console.log(ev);
  }
 async save(ev) {
    if (this.modalValues.id === undefined) {
      this.modalValues.criadopor = (await this.afAuth.currentUser).email
      this.modalValues.data_criacao = new Date();
      this.modalValues.id = config.getRandomId();
    }
    this.modalValues.update_data = new Date();

    this.dataSrv.saveTo(this.modalValues, config.url.dbAtividades);
    this.dialogRef.close();
  }
}
