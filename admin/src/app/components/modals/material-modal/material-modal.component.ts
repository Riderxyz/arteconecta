import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DataService } from "src/app/services/data.service";
import { config } from "../../../services/config";
import { ToastService } from "src/app/services/toast.service";
import { MaterialInterface } from "../../../models/material.interface";




@Component({
  templateUrl: './material-modal.component.html',
  styleUrls: ['./material-modal.component.scss']
})
export class MaterialModal implements OnInit {
  @ViewChild("ModalTitle", { static: true }) ModalTitle: ElementRef;
  titleTxt = "";
  modalValues: MaterialInterface = {} as any;
  sizetitleTxt = 0;
  headerColor = "";
  constructor(
    public dialogRef: MatDialogRef<MaterialModal>,
    @Inject(MAT_DIALOG_DATA) public data: MaterialInterface,
    private dataSrv: DataService,
    private toastSrv: ToastService,
  ) { }

  ngOnInit(): void {
    if (this.data === null) {
      console.clear();
      this.titleTxt = "ADICIONANDO";
      this.headerColor = "card-header-success";
      this.startTypingTitle();
    } else {
      this.titleTxt = "EDITANDO";
      this.modalValues = this.data;
      this.headerColor = "card-header-warning";
      this.startTypingTitle();
    }

  }

  startTypingTitle() {
    if (this.sizetitleTxt < this.titleTxt.length) {
      this.ModalTitle.nativeElement.innerHTML += this.titleTxt.charAt(
        this.sizetitleTxt
      );
      this.sizetitleTxt++;
      setTimeout(() => {
        this.startTypingTitle();
      }, 100);
    }
  }

  save(ev) {
    if (this.modalValues.id === undefined) {
     this.modalValues.id = config.getRandomId();
     }
    this.dataSrv.saveTo(this.modalValues, config.url.dbMateriais);
  this.dialogRef.close();
}
}
