import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { UsuarioInteface } from "../../../models/usuario.interface";

import { DataService } from "src/app/services/data.service";
import { config } from "../../../services/config";
import { AngularFireAuth } from "@angular/fire/auth";
import { ToastService } from "src/app/services/toast.service";
import { FormControl } from "@angular/forms";

interface ISexo {
  id: string;
  nome: string;
}
@Component({
  templateUrl: "./usuario-modal.component.html",
  styleUrls: ["./usuario-modal.component.scss"],
})
export class UsuarioModal implements OnInit {
  @ViewChild("ModalTitle", { static: true })
  ModalTitle: ElementRef;
  titleTxt = "";
  modalValues: UsuarioInteface = {} as any;
  sizetitleTxt = 0;
  headerColor = "";
  profissaoArr = [
    {
      value: "Arte-Educador(a)",
      name: "Arte-Educador(a)",
    },
    {
      value: "Arteterapeuta",
      name: "Arteterapeuta",
    },
    {
      value: "Estudante",
      name: "Estudante",
    },
    {
      value: "Professor(a)",
      name: "Professor(a)",
    },
    {
      value: "Psicólogo(a)",
      name: "Psicólogo(a)",
    },
    {
      value: "Outros",
      name: "Outros",
    },
  ];
  constructor(
    public dialogRef: MatDialogRef<UsuarioModal>,
    @Inject(MAT_DIALOG_DATA) public data: UsuarioInteface,
    private dataSrv: DataService,
    private toastSrv: ToastService,
    private afAuth: AngularFireAuth
  ) {}

  ngOnInit(): void {
    if (this.data === null) {
      this.titleTxt = "ADICIONANDO";
      this.headerColor = "card-header-success";
      this.startTypingTitle();
    } else {
      this.titleTxt = "EDITANDO";
      this.data.dt_cadastro = new Date(this.data.dt_cadastro);
      this.modalValues = this.data;

      setTimeout(() => {
        //   this.fillModalValues = this.data;
      }, 100);
      this.headerColor = "card-header-warning";
      this.startTypingTitle();
    }
  }

  startTypingTitle() {
    if (this.sizetitleTxt < this.titleTxt.length) {
      this.ModalTitle.nativeElement.innerHTML += this.titleTxt.charAt(
        this.sizetitleTxt
      );
      this.sizetitleTxt++;
      setTimeout(() => {
        this.startTypingTitle();
      }, 100);
    }
  }

  save(ev) {
    if (this.modalValues.id === undefined) {
      this.afAuth
        .createUserWithEmailAndPassword(
          this.modalValues.emailPrincipal,
          "123456"
        )
        .then((res) => {
          this.modalValues.updateDate = new Date();
          this.modalValues.id = res.user.uid;
          res.user.updateProfile({
            displayName: this.modalValues.nome,
          });
          this.dataSrv.saveTo(this.modalValues, config.url.dbUser);
          this.toastSrv.showToastSucess(
            "Usuario Incluido!",
            "O usuario " + this.modalValues.nome + " foi incluido com sucesso"
          );
        })
        .catch((err) => {
          this.toastSrv.showToastError(
            "Não foi possivel incluir o usuario",
            "Tente novamente mais tarde"
          );
        });
    } else {
      this.modalValues.updateDate = new Date();
      this.dataSrv.saveTo(this.modalValues, config.url.dbUser);
    }
    this.dialogRef.close();
  }

  set fillModalValues(dataFromGrid) {
    this.modalValues.id = dataFromGrid.id;
    this.modalValues.nome = dataFromGrid.nome;
    this.modalValues.sexo = dataFromGrid.sexo;
    this.modalValues.senha = dataFromGrid.senha;
    this.modalValues.dt_nascimento = dataFromGrid.dt_nascimento;
    this.modalValues.filhos = dataFromGrid.filhos;
    this.modalValues.profissao = dataFromGrid.profissao;
    this.modalValues.observacoes = dataFromGrid.observacoes;
    this.modalValues.endereco_uf = dataFromGrid.endereco_uf;
    this.modalValues.endereco_cidade = dataFromGrid.endereco_cidade;
    this.modalValues.endereco_pais = dataFromGrid.endereco_pais;
    this.modalValues.emailPrincipal = dataFromGrid.emailPrincipal;
    this.modalValues.celular = dataFromGrid.celular;
    this.modalValues.updateDate = dataFromGrid.updateDate;
    this.modalValues.updateBy = dataFromGrid.updateBy;
    this.modalValues.updateType = dataFromGrid.updateType;
    this.modalValues.ativo = dataFromGrid.ativo;
    this.modalValues.premium = dataFromGrid.premium;
    this.modalValues.dt_ativacaoPremium = dataFromGrid.dt_ativacaoPremium;
    this.modalValues.dt_cadastro = dataFromGrid.dt_cadastro;
  }
}
