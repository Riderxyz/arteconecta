import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioModal } from './usuario-inclusao.component';

describe('UsuarioModal', () => {
  let component: UsuarioModal;
  let fixture: ComponentFixture<UsuarioModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuarioModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
