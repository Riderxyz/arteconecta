import { Component, OnInit } from "@angular/core";
import { CentralRxJsService } from "../../services/centralRXJS.service";
import { config } from "../../services/config";
import { AuthService } from "src/app/services/auth.service";
import { Router } from "@angular/router";
import {
  flipOnEnterAnimation,
  flipOutXOnLeaveAnimation,
  hingeOnLeaveAnimation
} from "angular-animations";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  animations: [
    flipOnEnterAnimation(),
    flipOutXOnLeaveAnimation(),
    hingeOnLeaveAnimation()
  ]
})
export class HeaderComponent implements OnInit {
  showMenuButton = false;
  constructor(
    private centralRXJS: CentralRxJsService,
    public AuthSrv: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.events.subscribe(urlEvent => {
      if (this.router.url !== "/" && this.router.url !== "/login") {
        this.showMenuButton = true;
      } else {
        this.showMenuButton = false;
      }
    });
  }

  test() {
    this.router.navigateByUrl('/dashboard')
  }

  openSideMenu() {
    this.centralRXJS.sendData = { key: config.rxjsCentralKeys.openSideMenu };
  }
}
