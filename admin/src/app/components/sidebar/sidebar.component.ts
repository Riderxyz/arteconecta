import { Component, OnInit } from '@angular/core';
import { CargaService } from '../../services/carga.service';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { AuthService } from 'src/app/services/auth.service';
import { SidebarMenuInterface } from 'src/app/models/sidebarMenu.interface';
import { config } from 'src/app/services/config';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  arrPagesLinks: SidebarMenuInterface[] = [];
  constructor(
    public cargaService: CargaService,
    private centralRXJS: CentralRxJsService,
    private AuthSrv: AuthService
  ) { }

  ngOnInit() {
    this.arrPagesLinks = [
      {
        icon: 'fas fa-desktop',
        name: 'Home',
        path: '/dashboard'
      },
      {
        icon: 'fas fa-users',
        name: 'Usuarios',
        path: '/usuarios'
      },
      {
        icon: 'fas fa-file-alt',
        name: 'Propostas/Videoaulas',
        path: '/atividades',

      },
      {
        icon: 'fas fa-box',
        name: 'Materiais',
        path: '/materiais',
      },
      {
        icon: 'fas fa-check',
        name: 'Objetivos',
        path: '/objetivos',
      },
      {
        icon: 'fas fa-bell',
        name: 'Notificações',
        path: '/pushNoticications',

      },
      {

        icon: 'fas fa-tools',
        name: 'usuarios Admin',
        path: '/admin-management',

      },
      {
        icon: 'fas fa-sign-out-alt',
        name: 'Sair',
        path: '/login'
      }
    ];
  }

  jsonUsuarios() {
    this.cargaService.processarCargas();

  }
  async isLogOut(event: SidebarMenuInterface) {
    console.log('entrei aqui');
    setTimeout(() => {
      this.centralRXJS.sendData = { key: config.rxjsCentralKeys.openSideMenu };
    }, 70);
    if (event.path === '/login') {
      await this.AuthSrv.LogOut();
    }
  }
}
