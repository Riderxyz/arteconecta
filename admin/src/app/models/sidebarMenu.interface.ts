export interface SidebarMenuInterface {
  icon?: string;
  name: string;
  path: string;
}
