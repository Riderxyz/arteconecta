export interface UsuariosAntigo {
    IdCadastroSite: string;
    IdUser: string;
    IdOrigem: string;
    NomeCompleto: string;
    Apelido: string;
    Email: string;
    Email2: string;
    Celular: string;
    Sexo: string;
    Cidade: string;
    Estado: string;
    Pais: string;
    Profissao: string;
    Cargo: string;
    Filhos: string;
    Observacoes?: any;
    News: string;
    ContatoPrincipalEscola: string;
    UsuarioTeste: string;
    Excluido: string;
    DataHoraCadastro: string;
    AtualizadoPor: string;
    DataHoraAtualizacao: string;
}
