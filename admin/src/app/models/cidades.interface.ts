interface PaisInterface {
  estados: Estado[];
}

interface Estado {
  sigla: string;
  nome: string;
  cidades: string[];
}
