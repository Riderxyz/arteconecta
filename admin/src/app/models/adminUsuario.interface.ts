export interface AdminUsuarioInteface {
  id: string;
  nome: string;
  observacoes?: string;
  emailPrincipal: string;
  senha: string;
  celular?: string;
  updateDate: string | Date;
  updateBy: string;
  ativo?: string;
  dt_cadastro?: string | Date;
}
