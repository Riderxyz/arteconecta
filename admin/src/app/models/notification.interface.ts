export interface MessageInterface {
  pushMessage: NotificationInterface;
  token: string[];
}
export interface NotificationInterface {
     title: string;
     body: string;
     click_action?: string;
     icon?: string;
     color?: string;
     badge?: string;
     tap?: 'false' | 'true';
     tag?: string;

}

