export interface Root {
    admins: AdminInterface
  }
  
  export interface AdminInterface {
    nome: string
    email: string
    telefone: string
    ativo: boolean
    criadoEm: Date
    atualizadoEm: Date
    ultimoAcesso: Date
    log: Log
  }
  
  export interface Log {
    data: Date
    modulo: string
    acao: string
  }