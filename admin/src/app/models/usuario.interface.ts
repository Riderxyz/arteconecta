export interface UsuarioInteface {
    id: string;
    nome: string;
    sexo?: string;
    senha?: string;
    dt_nascimento?: string | Date;
    filhos?: string;
    favoriteList?: string[];
    profissao?: 'Arte-Educador(a)' | 'Arteterapeuta' | 'Estudante' | 'Professor(a)' | 'Psicólogo(a)' | 'Outros' | ' ';
    observacoes?: string;
    endereco_uf?: string;
    endereco_cidade?: string;
    endereco_pais?: string;
    emailPrincipal: string;
    celular?: string;
    updateDate: string | Date;
    updateBy: string;
    updateType?: string;
    ativo?: string;
    novoUsuario: boolean;
    notificationTokenList?: string[];
    podeEnviarNotificacao?: boolean;
    premium?: { token?: string; status: boolean; dt_ativacaoPremium?: Date; costumerId?: string; subscriptionsID?: string; };
    dt_ativacaoPremium?: string | Date;
    dt_cadastro?: string | Date;
} 