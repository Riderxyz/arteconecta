import { AtividadeInterface } from "./atividade.interface";
import { MaterialInterface } from "./material.interface";
import { ObjetivoInterface } from "./objetivo.interface";
import { UsuarioInteface } from "./usuario.interface";

export interface ArteConectaInterface {
  usuarios: UsuarioInteface[];
  atividades: AtividadeInterface[];
  base_material: MaterialInterface[];
  base_objetivo: ObjetivoInterface[];
}




// export interface Listaobjetivo {
//     id: string;
//     objetivo?: string;
// }

// export interface Listamaterial {
//     id: string;
//     material?: string;
// }

// export interface Listaatividade {
//     atividade?: string;
// }

