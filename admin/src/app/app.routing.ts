import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AuthGuardService } from './services/auth.guard';
import { DashboardPage } from './pages/dashboard/dashboard.page';
import { LoginPage } from './pages/login/login.page';
import { UsuariosPage } from './pages/usuarios/usuarios.page';
import { AtividadesPage } from './pages/atividades/atividades.page';
import { MateriaisPage } from './pages/materiais/materiais.page';
import { ObjetivosPage } from './pages/objetivos/objetivos.page';
import { NotificacoesPage } from './pages/notificacoes/notificacoes.page';
import { AdminManagementPage } from './pages/admin-management/admin-management.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',

  },
  {
    path: 'login',
    component: LoginPage,
  },
  {
    path: 'dashboard',
    component: DashboardPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'usuarios',
    component: UsuariosPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'atividades',
    component: AtividadesPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'materiais',
    component: MateriaisPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'objetivos',
    component: ObjetivosPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'pushNoticications',
    component: NotificacoesPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin-management',
    component: AdminManagementPage,
    canActivate: [AuthGuardService]
  },

];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [
  ],
})
export class RoutingModule { }
