import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { pushNotification } from "./pushNotification/pushNotification";
import { DeletarUsuario } from "./util/deletarUsuario";
 import { enviarEmail } from "./enviarEmail/enviarEmail";

import sgMail from "@sendgrid/mail";
admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: "https://arteconecta-27292.firebaseio.com",
});

const API_KEY = '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';
sgMail.setApiKey(API_KEY);

export const SendNotification = functions.https.onRequest(
  (request, response) => {
    pushNotification(request, response);
  }
);
export const onUserDelete = functions.auth.user().onDelete(async (event) => {
  await DeletarUsuario(event);
});

export const onEmailRequest = functions.https.onRequest(async (request, response) => {
 
   await enviarEmail(request, response, sgMail)
});
