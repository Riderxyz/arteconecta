import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import Stripe from "stripe";
const cors = require("cors")({ origin: true });
export const cancelarPremium = (
  request: functions.https.Request,
  response: functions.Response
) => {
  /* const event: functions.database.DataSnapshot = ev; */
  cors(request, response, () => {
    const stripe = new Stripe("sk_test_n8lTj1SrkKlSACAJDeBFMBdh00e478E5QW", {
      apiVersion: "2020-08-27",
      maxNetworkRetries: 3,
    });
    const subscribeId = request.body.subscribeId;
    const userId = request.body.userId;
    stripe.subscriptions
      .del(subscribeId)
      .then((subscriptionsDel) => {
        admin
          .database()
          .ref(`/ArteConecta/usuario/${userId}/premium/`)
          .update({
            subscriptionsID: "cancelado",
            status: false,
          })
          .then(() => {
            response.status(200).send(subscriptionsDel);
          })
          .catch((err) => {
            response.status(500).send(err);
          });
      })
      .catch((err) => {
        response.status(500).send(err);
      });
  });
};
