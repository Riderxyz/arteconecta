import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import Stripe from "stripe";
const cors = require("cors")({ origin: true });
export const mudarParaPremium = (
  request: functions.https.Request,
  response: functions.Response
) => {
  /* const event: functions.database.DataSnapshot = ev; */


  cors(request, response, () => {
    const stripe = new Stripe("sk_test_n8lTj1SrkKlSACAJDeBFMBdh00e478E5QW", {
      apiVersion: "2020-08-27",
      maxNetworkRetries: 3,
    });
    const currentToken = request.body.token;
    const userCostumerId = request.body.costumerId;
    const userId = request.body.userId;
    
    if (!currentToken) {
      throw new Error("sem token para incrição");
    }
    stripe.subscriptions
      .create({
        customer: userCostumerId,
        items: [
          {
            plan: "plan_H5edtrVuGE0gcB",
          },
        ],
      })
      .then((subscriptions) => {
        admin
          .database()
          .ref(`/ArteConecta/usuario/${userId}/premium/`)
          .update({
            subscriptionsID: subscriptions.id,
            status: true,
            dt_ativacaoPremium: "" + new Date() + "",
          })
          .then(() => {
            response.status(200).send(subscriptions);
          })
          .catch((err) => {
            response.status(500).send(err);
          });
      })
      .catch((err) => {
        response.status(500).send(err);
      });

  })
};
