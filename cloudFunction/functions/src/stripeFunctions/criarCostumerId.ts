import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import Stripe from "stripe";
import { UsuarioInteface } from "../util/userDetails.interface";
const cors = require("cors")({ origin: true });
export const criarCostumerId = (
  request: functions.https.Request,
  response: functions.Response
) => {
  /* const event: functions.database.DataSnapshot = ev; */
  cors(request, response, () => {
    const stripe = new Stripe("sk_test_n8lTj1SrkKlSACAJDeBFMBdh00e478E5QW", {
      apiVersion: "2020-08-27",
      maxNetworkRetries: 3,
    });
    const userObj: UsuarioInteface = request.body.userObj;

    console.log("Dentro da função");

    stripe.customers
      .create({
        email: userObj.emailPrincipal,
        name: userObj.nome,
        phone: userObj.celular,
      })
      .then((newCostumer) => {
        admin
          .database()
          .ref(`/ArteConecta/usuario/${userObj.id}/premium/`)
          .update({
            costumerId: newCostumer.id,
          })
          .then(() => {
            console.log(newCostumer);

            response.send(newCostumer);
          })
          .catch((err) => {
            console.log(err);
            response.send(err);
          });
      })
      .catch((err) => {
        console.log("erro ao criar novo usuario");
        response.send(err);
      });
  });
};
