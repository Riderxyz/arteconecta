import * as functions from "firebase-functions";
import Stripe from "stripe";
import * as admin from "firebase-admin";
const cors = require('cors')({origin: true});
export const cadastrarCartao = (
  request: functions.https.Request,
  response: functions.Response
) => {
  /* const event: functions.database.DataSnapshot = ev; */
  cors(request, response, () => {
    const stripe = new Stripe("sk_test_n8lTj1SrkKlSACAJDeBFMBdh00e478E5QW", {
      apiVersion: "2020-08-27",
      maxNetworkRetries: 3,
    });
    const currentToken = request.body.token;
    const userCostumerId= request.body.costumerId;
    const userId = request.body.userId;
    console.log('Dentro da função');
    
   stripe.customers.update(userCostumerId, {
      source: currentToken,
    }).then(async (userCard) => {
     await admin
      .database()
      .ref(`/ArteConecta/usuario/${userId}/premium/`)
      .update({
        token: currentToken,
        status: false
      })
      
      console.log('Funcionou!', userCard);
      response.status(200).send(userCard)
    }).catch((err) => {
      console.log('ERRO', err);
        response.status(500).send(err)
    })
  })
}
