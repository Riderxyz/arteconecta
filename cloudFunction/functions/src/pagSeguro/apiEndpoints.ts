const apiEndpoins = {
    idPlano: 'A2F8655D-C0C0-DBD6-6486-0F9F12C05243',
    creditCardToken: 'https://df.uol.com.br/v2/cards',
    assinarPlano: ((email: string, tokenSandbox: string) => {
        return 'https://ws.sandbox.pagseguro.uol.com.br/pre-approvals?email='
            + email + '&token='
            + tokenSandbox
    })
}