export interface PagSeguro {
    plan: string;
    reference: string;
    sender: Sender;
    paymentMethod: PaymentMethod;
}

export interface PaymentMethod {
    type: string;
    creditCard: CreditCard;
}

export interface CreditCard {
    token: string;
    holder: Holder;
}

export interface Holder {
    name: string;
    birthDate: string;
    documents: Document[];
    phone: Phone;
    billingAddress: Address;
}

export interface Sender {
    name: string;
    email: string;
    hash?: string;
    phone: Phone;
    address: Address;
    documents: Document[];
}

export interface Document {
    type: 'CPF' | 'CNPJ';
    value: string;
}

export interface Address {
    street: string;
    number: string;
    complement: string;
    district: string;
    city: string;
    state: string;
    country: string;
    postalCode: string;
}

export interface Phone {
    areaCode: string;
    number: string;
}