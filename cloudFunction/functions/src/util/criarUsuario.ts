import * as admin from "firebase-admin";
import Stripe from "stripe";
export const criarUsuario = (async (user: admin.auth.UserRecord) => {
  const stripe = new Stripe("sk_test_n8lTj1SrkKlSACAJDeBFMBdh00e478E5QW", {
    apiVersion: "2020-08-27",
    maxNetworkRetries: 3,
  });

  stripe.customers
    .create({
      email: user.email,
    })
    .then(async (costumer) => {
      const updates: any = {};
      console.log('O que vem aqui', costumer);


      const newUser = {
        id: user.uid,
        nome: user.displayName === ""|| user.displayName === undefined ? user.email:user.displayName,
        emailPrincipal: user.email,
        photo: user.photoURL === ""|| user.photoURL === undefined ? " ":user.photoURL ,
        telefone: user.phoneNumber,
        premium: {
          status: false,
          costumerId: costumer.id
        },
        dt_cadastro: '' + new Date() + '',
        updateDate: '' + new Date() + '',
        updateBy: user.displayName === ""|| user.displayName === undefined ? user.email:user.displayName,
      }
      updates[`/ArteConecta/usuario/${user.uid}`] = newUser;
      /* updates[`/ArteConecta/${costumer.id}`] = user.uid;  */
      console.log(updates);

      return admin.database().ref().update(updates);
    })
    .catch((err) => {
      console.log('Erro em criação de costumer para o Stripe', err);

    });
});
