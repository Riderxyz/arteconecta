export interface UsuarioInteface {
    id: string;
    nome: string;
    apelido: string;
    sexo: string;
    senha: string;
    dt_nascimento: string | Date;
    filhos: string;
    favoriteList: string[];
    profissao: string;
    cargo: string;
    observacoes: string;
    photo: string;
    endereco_tipo: string;
    endereco_logradouro: string;
    endereco_bairro: string;
    endereco_cep: string;
    endereco_uf: string;
    endereco_cidade: string;
    endereco_pais: string;
    emailPrincipal: string;
    emailAuxiliar: string;
    telefone: string;
    celular: string;
    updateDate: string | Date;
    updateBy: string;
    updateType: string;
    ativo: string;
    notificationTokenList?: string[];
    premium: { token: string; status: boolean; dt_ativacaoPremium: Date; costumerId: string; subscriptionsID: string; }
    dt_ativacaoPremium: string | Date;
    dt_cadastro?: string | Date;
}
