export interface EmailInterface {
    assunto: string;
    corpo: string;
    corpoHtml?: string;
    destinatarios: string;
    remetente: string;
}