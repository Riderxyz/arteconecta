import * as functions from "firebase-functions";
import { MailDataRequired, MailService } from "@sendgrid/mail";
const cors = require("cors")({ origin: true });

export const enviarEmail = async (
  request: functions.https.Request,
  response: functions.Response,
  transporter: MailService
) => {
  cors(request, response, () => {
    transporter.setSubstitutionWrappers("{{", "}}");
    console.log("Rodando direto do index.ts e não da função");
    console.log(JSON.stringify(request.body));

    const msg:MailDataRequired = {
      to: request.body.emailUsuario,
      from: 'contato@arteacaobrasil.com.br',
     // subject: request.body.assunto,
      templateId: "d-6d18a44e3f544c16afe957d3162db56d",
      substitutionWrappers: ["{{", "}}"],
      dynamicTemplateData: {
        nomeUsuario:request.body.nomeUsuario,
        emailUsuario: request.body.emailUsuario,
        textoFeedback: request.body.textoFeedback
      },
    };
    return transporter
      .send(msg)
      .then((res) => {
        response.status(200).send(res);
      })
      .catch((err) => {
        const error = {
          text: "Error 500",
          err: JSON.stringify(err),
          //config: functions.config(),
        };
        response.status(500).send(error);
      });
  });
};

