import * as listaAtividadesAntigas from "../../database/Atividades.json";
import uuid from "uuid-random";
import { AtividadeInterface } from "../models/atividade.interface";
export const carregarAtividades = (async () => {

    const x: AtividadeInterface[] = [] as AtividadeInterface[];
    const lista = (listaAtividadesAntigas as any).default;
    console.log((listaAtividadesAntigas as any).default.length);
    var cont = 0;
    lista.forEach((element:any, index: number) => {
        if (element.Excluido === "0") {
            const a: AtividadeInterface = {} as AtividadeInterface;
            a.id = uuid();
            if (element.Edicao !== undefined && element.Edicao !== null) {
                if (element.Edicao === "1") {
                  a.tipo = "VideoAula";
                } else {
                  a.tipo = "Proposta";
                }
              } else {
                a.tipo = "Proposta"
              }

              a.titulo =
            element.Titulo === undefined || element.Titulo === ""
              ? " "
              : element.Titulo;
          a.update_autor =
            element.AtualizadoPor === undefined || element.AtualizadoPor === ""
              ? " "
              : element.AtualizadoPor;
          a.update_data =
            element.DataHoraAtualizacao === undefined ||
              element.DataHoraAtualizacao === ""
              ? " "
              : element.DataHoraAtualizacao;
          a.update_observacao = " ";
          a.update_tipo = " ";
          a.videourl =
            element.URLvideoEmbed === undefined || element.URLvideoEmbed === ""
              ? " "
              : element.URLvideoEmbed;
          a.lista_atividade = [];

          a.maturidadeMin =
            element.MaturidadeMin === undefined || element.MaturidadeMin === ""
              ? " "
              : element.MaturidadeMin;
          a.maturidadeMax =
            element.MaturidadeMax === undefined || element.MaturidadeMax === ""
              ? " "
              : element.MaturidadeMax;
          // tslint:disable-next-line: max-line-length
          a.numeroParticipantes =
            element.IdParticipantes === undefined ||
              element.IdParticipantes === ""
              ? " "
              : element.IdParticipantes;
          a.descricao =
            element.Descricao === undefined || element.Descricao === ""
              ? " "
              : element.Descricao;
          a.sugestoes =
            element.Sugestões === undefined || element.Sugestões === ""
              ? " "
              : element.Sugestões;
          a.imagem =
            element.Imagem1 === undefined || element.Imagem1 === ""
              ? " "
              : element.Imagem1;
          a.legendasVideo =
            element.LegendaVideo === undefined || element.LegendaVideo === ""
              ? " "
              : element.LegendaVideo;
          a.numedicao =
            element.numEdicao === undefined || element.numEdicao === ""
              ? " "
              : element.numEdicao;

          a.criadopor =
            element.CriadoPor === undefined || element.CriadoPor === ""
              ? " "
              : element.CriadoPor;
          a.data_criacao =
            element.DataHoraCriacao === undefined ||
              element.DataHoraCriacao === ""
              ? " "
              : element.DataHoraCriacao;
          a.excluido = false;

          if (element.IndicadoPais !== undefined || element.IndicadoPais !== "") {
            if (element.IndicadoPais === "0") {
              a.indicacaopais = false;
            } else {
              a.indicacaopais = true;
            }
          }
          if (element.Edicao !== undefined || element.Edicao !== "") {
            if (element.Edicao === "0") {
              a.edicao = false;
            } else {
              a.edicao = true;
            }
          }
          if (element.Free !== undefined || element.Free !== "") {
            if (element.Free === "0") {
              a.free = false;
            } else {
              a.free = true;
            }
          }
          if (element.Publicado !== undefined || element.Publicado !== "") {
            if (element.Publicado === "0") {
              a.publicado = false;
            } else {
              a.publicado = true;
            }
          }
          if (element.Revisado !== undefined || element.Revisado !== "") {
            if (element.Revisado === "0") {
              a.revisao = false;
            } else {
              a.revisao = true;
            }
          }

          a.lista_material =
            element.Materiais === undefined || element.Materiais === ""
              ? " "
              : element.Materiais;
          a.lista_objetivo =
            element.Objetivos === undefined || element.Objetivos === ""
              ? " "
              : element.Objetivos;
          let transforMat: any = a.lista_material;
          let transformObj: any = a.lista_objetivo;
          let resultMatArr: any[] = [];
          let resultObj: any[] = [];
          if (transforMat !== null) {
            transforMat = transforMat.split(/\s*,\s*/);
            transforMat.forEach((l, index) => {
              if (this.getMaterialId(l)) {
                resultMatArr.push(this.getMaterialId(l));
              }
              if (index === transforMat.length - 1) {
                a.lista_material = resultMatArr;
              }
            });
          }
          if (transformObj !== null) {
            transformObj = transformObj.split(/\s*,\s*/);
            transformObj.forEach((l, index) => {
              if (this.getObjetivoId(l)) {
                resultObj.push(this.getObjetivoId(l));
              }
              if (index === transformObj.length - 1) {
                a.lista_objetivo = resultObj;
              }
            });
          }
          x.push(a);
        } 
    })
})