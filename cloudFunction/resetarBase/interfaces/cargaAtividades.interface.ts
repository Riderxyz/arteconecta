export interface CargaAtividadeInterface {
    id?: string;
    origem: string;
    tipo?: string;
    titulo?: string;
    lista_atividade: string[];
    lista_material: string[];
    lista_objetivo: string[];
    maturidadeMin?: number;
    maturidadeMax?: number;
    numeroParticipantes?: string;
    descricao?: string;
    sugestoes?: string;
    comentarios?: string;
    legendasVideo?: string;
    kit: Kit;
    imagem?: string;
    videourl?: string;
    edicao?: string;
    numedicao?: number;
    indicacaopais?: boolean;
    free?: boolean;
    partipantes?: string;
    revisao?: string;
    publicado?: string;
    excluido?: boolean;
    atividadesassociadas: Atividadesassociada[];
    update_data?: Date;
    update_autor?: string;
    update_tipo?: string;
    update_observacao?: string;
    data_criacao: Date;
    criadopor: string;
  }

  export interface Atividadesassociada {
    associacaoatividadeid?: string;
    dataassociacao?: Date;
    associadapor?: string;
  }

  export interface Kit {
    tipo: Tipo[];
  }
  
  export interface Tipo {
    descricaoKit?: string;
    materiais?: string;
    instrucoes?: string;
  }