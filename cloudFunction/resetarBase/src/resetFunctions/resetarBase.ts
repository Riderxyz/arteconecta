import * as admin from "firebase-admin";
import {carregarAtividades} from './carregarAtividades';
import {carregarUsuario} from './carregarUsuario';
import {carregarMateriais} from './carregarMateriais';
import {carregarObjetivos} from './carregarObjetivos';

//import { UsuariosAntigo } from "../models/usuarioAntigo.interface";
export const resetarBaseDeDados = (() => {
            let cargaUsuario = [] as any[];
            let cargaMateriais = [] as any[];
            let cargaObjetivos = [] as any[];
            let cargaAtividades = [] as any[];

            const processarCarga = (async ()=> {
                await carregarUsuario();
                await carregarMateriais();
                await carregarObjetivos();
             //  const res = await carregarAtividades();
            //   admin.database().ref(`/ArteConecta/`).set(res);
            })

            processarCarga()
    })