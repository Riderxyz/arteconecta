import { NgModule, Injector } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { TextMaskModule } from 'angular2-text-mask';
// modulos das paginas
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing";
import { AuthModule } from "src/page/authModule/authModule.module";
import { UserInfoModule } from "src/page/userInfo/userInfo.module";

// AngularFire
import { environment } from "./../environments/environment";
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireMessagingModule } from "@angular/fire/messaging";
import { AngularFireStorageModule } from "@angular/fire/storage";

// Plugins
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook } from "@ionic-native/facebook/ngx";
import { IonicStorageModule } from "@ionic/storage";
import { Stripe } from "@ionic-native/stripe/ngx";
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
// Services
import { LocalStorageService } from "../services/localStorage.service";
import { DataService } from "../services/data.service";
import { HttpClientModule } from "@angular/common/http";
import { CleanerService } from "../services/cleaner.service";
import { ToastService } from "../services/toast.service";
import { PushNotificationService } from "../services/pushNotification.service";
import { PaymentService } from "../services/payment.service";
import { AuthService } from "../services/auth.service";

import { ComponentModule } from "../components/components.module";
import { DirectiveModule } from "./../directive/directive.module";

const Plugins = [GooglePlus, Facebook];

export let AppInjector: Injector;

const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule,
];
const FormModules = [ReactiveFormsModule, FormsModule];
const Modules = [ComponentModule, DirectiveModule];
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ...AngularFire,
    ...FormModules,
    AuthModule,
    UserInfoModule,
    HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    TextMaskModule,
    ...Modules,
  ],
  providers: [
    StatusBar,
    ...Plugins,
    SplashScreen,
    LocalStorageService,
    DataService,
    CleanerService,
    AuthService,
    ToastService,
    PushNotificationService,
    PaymentService,
    Stripe,
    FirebaseX,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private injector: Injector) {
    AppInjector = this.injector;
  }
}
