import { DataService } from "./../services/data.service";
import { Component } from "@angular/core";
import { Platform, NavController, MenuController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { AuthService } from "../services/auth.service";
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError,
} from "@angular/router";
import { CleanerService } from "../services/cleaner.service";
import { config } from "../services/config";
import * as firebase from "firebase/app";
import * as moment from "moment";
import { timer } from "rxjs";

import {
  zoomInOnEnterAnimation,
  bounceOutOnLeaveAnimation,
} from "angular-animations";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
  animations: [zoomInOnEnterAnimation(), bounceOutOnLeaveAnimation()],
})
export class AppComponent {
  arrBtn = [];
  showSplash = true;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    public AuthSrv: AuthService,
    private dataSrv: DataService,
    private router: Router
  ) {
    moment.locale("pt-br");
    this.AuthSrv.currentUserObservable.subscribe(async (user) => {
      if (user) {
        // await this.isUserDataAvailable(user);
        this.dataSrv.getSpecificUser(user.uid).subscribe((res) => {
          if (res === null) {
            //    this.AuthSrv.currentUser = res
            this.AuthSrv.setNewUserData(user);
            this.navCtrl.navigateRoot("userInfo/userDetail");
          } else {
            this.AuthSrv.currentUser = res;
            if (this.AuthSrv.currentUser.novoUsuario) {
              this.navCtrl.navigateRoot("userInfo/userDetail");
            } else {
              this.navCtrl.navigateRoot("/home");
            }
          }
        });
      } else {
          this.navCtrl.navigateRoot("auth/login");
      }
    });
    this.initializeApp();
    //this.isMenuEnable()
    router.events.subscribe((val: Event) => {
      if (val instanceof NavigationEnd) {
        if (val.url === "/auth/login") {
          this.isMenuEnable(false);
        } else {
          this.isMenuEnable(true);
        }
      }
    });
    this.platform.backButton.subscribe(async () => {
      if (
        (this.router.isActive("/home", true) && this.router.url === "/home") ||
        (this.router.isActive("/auth/login", true) &&
          this.router.url === "/auth/login")
      ) {
        // tslint:disable-next-line: no-string-literal
        navigator["app"].exitApp();
      }
    });
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault;
      /* this.statusBar.overlaysWebView(true); */
      this.splashScreen.hide();
      timer(4000).subscribe(() => (this.showSplash = false));
      this.arrBtn = [
        {
          name: "Início",
          path: "/home",
          icon: "home",
        },
        {
          name: "Sobre",
          path: "/sobre",
          icon: "information-circle",
        },
        {
          name: "Sua Conta",
          path: "/userInfo/userResume",
          icon: "finger-print",
        },
        /*         {
                  name: 'Premium',
                  path: '/userInfo/userCreditCard',
                  icon: 'star'
                }, */
        {
          name: "ajuda & feedback",
          path: "/ajuda&Feedback",
          icon: "help-circle",
        },
        {
          name: "sair",
          path: "",
          icon: "exit",
        },
      ];
    });
  }

  async isMenuEnable(value) {
    this.menuCtrl.enable(value);
  }
  goTo(event) {
    if (event.name === "sair") {
      this.AuthSrv.LogOut();
    } else {
      this.navCtrl.navigateRoot(event.path);
    }
  }
}
