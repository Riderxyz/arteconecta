import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ComponentModule } from 'src/components/components.module';
import { AjudaFeedbackPage } from './ajudaFeedback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    RouterModule.forChild([
      {
        path: '',
        component: AjudaFeedbackPage
      }
    ])
  ],
  declarations: [AjudaFeedbackPage],
})
export class AjudaFeedbackModule { }
