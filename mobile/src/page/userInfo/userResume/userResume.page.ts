import {
  Component,
  AfterViewInit,
  OnInit,
} from "@angular/core";
/* import { config } from "../../../services/config"; */
/* import { PushNotificationService } from "../../../services/pushNotification.service"; */
/* import { LocalStorageService } from "../../../services/localStorage.service"; */
import { AuthService } from "../../../services/auth.service";
import { UsuarioInteface } from './../../../models/usuario.interface';
import * as moment from "moment";
import { NavController, AlertController } from "@ionic/angular";
import { PaymentService } from "src/services/payment.service";
import {
  fadeInOnEnterAnimation,
  fadeOutOnLeaveAnimation,
  fadeInUpOnEnterAnimation,
} from "angular-animations";
@Component({
  templateUrl: "./userResume.page.html",
  styleUrls: ["./userResume.page.scss"],
  animations: [
    fadeInOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
    fadeInUpOnEnterAnimation(),
  ],
})
export class UserResumePage implements AfterViewInit, OnInit {
  userData: UsuarioInteface;
  showLoad = false;
  showPrice = false;
  premiumPrice = "16,90";
  constructor(
    private AuthSrv: AuthService,
    public navCtrl: NavController,
    private paymentSrv: PaymentService,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.userData = this.AuthSrv.currentUser;
  }

  ngAfterViewInit() {
/*     this.userData.dt_nascimento = moment(this.userData.dt_nascimento).format(
      "L"
    );
    console.log(this.userData.dt_cadastro);
    const dataCadastro = moment(this.userData.dt_cadastro).format("L");
    if (dataCadastro !== "Invalid Date") {
      this.userData.dt_cadastro = moment(this.userData.dt_cadastro).format("L");
    } */
  }

  get isUserPremium() {
    if (this.userData.premium.status) {
      return true;
    } else {
      return false;
    }
  }

  async cancelPremium() {
    const alert = await this.alertController.create({
      header: "Cancelar versão premium!",
      message: "Tem certeza que quer cancelar sua inscrição?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Sim",
          handler: () => {
            console.log("Confirm Okay");
            this.paymentSrv
              .cancelPremium(this.userData.premium.costumerId, this.userData.id)
              .subscribe((res) => {
                this.navCtrl.navigateRoot("/home");
              });
          },
        },
      ],
    });
    await alert.present();
  }
  get getPremiumTime(): number {
    const premiumDate = new Date(
      this.userData.premium.dt_ativacaoPremium
    ).getTime();
    const today = new Date().getTime();
    const diferencaEmTempo = today - premiumDate;
    const result = diferencaEmTempo / (1000 * 3600 * 24);
    return Math.round(result);
  }

  goTo() {
    this.showPrice = false;
    setTimeout(() => {
      this.navCtrl.navigateForward("userInfo/userCreditCard");
    }, 200);
  }
  /*   get isPushAvailable(): boolean {
      if (this.userData.notificationTokenList === undefined) {
        return false;
      } else {
        return true;
      }
    }
    onPushTokenRequest(event) {
      console.log('Olaaaa', event.detail.checked);
      if (event.detail.checked) {
        this.pushSrv.getPhoneToken().then(() => {
          console.log('token adicionado', this.AuthSrv.currentUser);
        });
    }
  } */
}
