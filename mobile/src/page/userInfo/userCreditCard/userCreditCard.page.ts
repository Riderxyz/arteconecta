import {
  Component,
  AfterViewInit,
  OnInit,
} from '@angular/core';
import {
  Stripe,
  StripeCardTokenRes,
  StripeCardTokenParams,
} from '@ionic-native/stripe/ngx';
import { config } from 'src/services/config';
import { AuthService } from 'src/services/auth.service';
import { ModalController, LoadingController } from '@ionic/angular';
import { ModalCheckoutComponent } from 'src/components/modal-checkout/modal-checkout.component';
import { PaymentService } from 'src/services/payment.service';
@Component({
  templateUrl: './userCreditCard.page.html',
  styleUrls: ['./userCreditCard.page.scss'],
})
export class UserCreditCardPage implements AfterViewInit, OnInit {
  messages = { validDate: 'valid\ndate', monthYear: 'mm/yyyy' };
  placeholders = {
    number: '•••• •••• •••• ••••',
    name: 'Full Name',
    expiry: '••/••',
    cvc: '•••',
  };
  creditCard: StripeCardTokenParams = null;
  customExpiry = '';
  backgroundColorCard: any = '';
  constructor(
    private stripe: Stripe,
    private AuthSrv: AuthService,
    private paymentSrv: PaymentService,
    private loadingController: LoadingController,
    public modalCtrl: ModalController
  ) {}

  ngOnInit(): void {
    this.stripe.setPublishableKey(config.api.key.stripeKey);

    this.creditCard = {
      number: '',
      name: '',
      expMonth: 0,
      expYear: 0,
      cvc: '',
    };
  }
  ngAfterViewInit() {
    this.creditCard = {
      number: '4242424242424242',
      name: 'Iago Favilla',
      expMonth: 11,
      expYear: 2020,
      cvc: '240',
    };
  }

  async makePayment(ev) {
    const t1 = this.customExpiry.indexOf(' / ');
    const res1 = this.customExpiry.substring(0, t1);
    const res2 = this.customExpiry.substring(t1 + 3, this.customExpiry.length);
    /*    this.creditCard.expMonth = res1 as any;
       this.creditCard.expYear = res2 as any; */
    /* this.creditCard.name = this.AuthSrv.currentUser.nome; */
    this.creditCard.address_state = this.AuthSrv.currentUser.endereco_uf;
    this.creditCard.address_city = this.AuthSrv.currentUser.endereco_cidade;
    this.creditCard.address_country = this.AuthSrv.currentUser.endereco_pais;
/*     this.creditCard.address_line1 = this.AuthSrv.currentUser.endereco_logradouro;
    this.creditCard.address_line2 = this.AuthSrv.currentUser.endereco_cep;
    this.creditCard.postalCode = this.AuthSrv.currentUser.endereco_cep; */
  }

  async startStripe() {
    const token = ''; //await this.stripe.createCardToken(this.creditCard);

    const loading = await this.loadingController.create({
      message: '',
      spinner: 'bubbles',
    });
    await loading.present();
    await loading.dismiss();
    const modal = await this.modalCtrl.create({
      component: ModalCheckoutComponent,
      componentProps: {
        checkOutObj: this.creditCard,
        token,
      },
    });
    await modal.present();
/*     this.paymentSrv
      .changeUserCreditCard(token.id, this.AuthSrv.currentUser.premium.costumerId, this.AuthSrv.currentUser.id)
      .subscribe(
        async (userStripeData) => {
          console.log(userStripeData);
          await loading.dismiss();
          const modal = await this.modalCtrl.create({
            component: ModalCheckoutComponent,
            componentProps: {
              checkOutObj: this.creditCard,
              token,
            },
          });
          await modal.present();
        },
        async (err) => {
          await loading.dismiss();
          console.log('Erro', err);
        }
      ); */
  }
  onCreditCardChange() {
    const cardElement = document.getElementsByClassName('jp-card-front');
    const color = window.getComputedStyle(cardElement[0], ':before');
    this.backgroundColorCard = {
      'background-color': color.backgroundColor,
    };
  }
}
