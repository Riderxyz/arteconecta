import { AngularFireDatabase } from "@angular/fire/database";
import { async } from "@angular/core/testing";
import { Component, OnInit, AfterContentInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { DataService } from "../../../services/data.service";
import { ToastService } from "../../../services/toast.service";
import { UsuarioInteface } from "./../../../models/usuario.interface";
import { AuthService } from "../../../services/auth.service";
import { config } from "../../../services/config";
import { AlertController, NavController } from "@ionic/angular";

@Component({
  templateUrl: "./userEdit.page.html",
  styleUrls: ["./userEdit.page.scss"],
})
export class UserEditPage implements OnInit, AfterContentInit {
  userInfo: UsuarioInteface = {} as any;
  estados = {
    sigla: [],
    nome: [],
    cidades: [],
  };
  // isNewUser = true;
  isLoading = true;
  profissaoArr = [
    {
      value: "Arte-Educador(a)",
      name: "Arte-Educador(a)",
    },
    {
      value: "Arteterapeuta",
      name: "Arteterapeuta",
    },
    {
      value: "Estudante",
      name: "Estudante",
    },
    {
      value: "Professor(a)",
      name: "Professor(a)",
    },
    {
      value: "Psicólogo(a)",
      name: "Psicólogo(a)",
    },
    {
      value: "Outros",
      name: "Outros",
    },
  ];
  outraProfissao: any = " ";
  selectSexo: any = {
    header: "Sexo",
    subHeader: "Selecione o sexo com o qual se identifica",
  };
  selectProfissao: any = {
    header: "Escolha uma das profissões abaixo",
  };
  masks: any = {};
  constructor(
    private dataSrv: DataService,
    public AuthSrv: AuthService,
    private alertController: AlertController,
    private navCtrl: NavController,
    public db: AngularFireDatabase
  ) {
    config.estados.forEach((element) => {
      this.estados.sigla.push(element.sigla);
    });
  }

  ngOnInit(): void {
    this.masks = {
      phoneNumber: [
        "(",
        /[1-9]/,
        /\d/,
        ")",
        " ",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ],
      cardNumber: [
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ],
      cardExpiry: [/[0-1]/, /\d/, "/", /[1-2]/, /\d/],
      orderCode: [/[a-zA-z]/, ":", /\d/, /\d/, /\d/, /\d/],
    };
  }
  ngAfterContentInit() {
    this.releaseLoading();
  }

  releaseLoading() {
    if (this.AuthSrv.currentUser !== null) {
      this.isLoading = false;
      this.userInfo = this.AuthSrv.currentUser;
      if (this.AuthSrv.currentUser.novoUsuario) {
        this.alertController
          .create({
            header: "Bem vindo!",
            message:
              "Vamos precisar que complete alguns dados de cadastro para iniciarmos o app.",
            buttons: [
              {
                text: "Okay",
                role: "cancel",
                cssClass: "secondary",
                handler: () => {
                  console.log("confirmado");
                },
              },
            ],
          })
          .then((alert) => {
            alert.present();
            this.userInfo.podeEnviarNotificacao = true;
          });
      }
    } else {
      this.releaseLoading();
    }
  }

  save(ev: NgForm) {
    if (this.userInfo.profissao !== "Outros") {
      this.userInfo.outraProfissao = null;
    }

    if (this.AuthSrv.currentUser.novoUsuario) {
      this.navCtrl.navigateBack("home");
      this.AuthSrv.currentUser.novoUsuario = false;
      this.dataSrv.saveTo(this.userInfo, config.url.dbUser);
    } else {
      this.dataSrv.saveTo(this.userInfo, config.url.dbUser);
      this.navCtrl.navigateBack("userInfo/userResume");
    }
  }
}
