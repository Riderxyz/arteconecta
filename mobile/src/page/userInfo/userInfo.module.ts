import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ComponentModule } from 'src/components/components.module';
import { UserEditPage } from './userDetail/userEdit.page';
import { UserCreditCardPage } from './userCreditCard/userCreditCard.page';
import { CardModule } from 'ngx-card/ngx-card';
import { UserResumePage } from './userResume/userResume.page';
import {  HttpClientModule } from '@angular/common/http';
import { TextMaskModule } from 'angular2-text-mask';

const routes: Routes = [
    {
        path: 'userDetail',
        component: UserEditPage
    },
    {
        path: 'userResume',
        component: UserResumePage
    },
    {
        path: 'userCreditCard',
        component: UserCreditCardPage
    },
];
@NgModule({
    declarations: [
        UserEditPage,
        UserCreditCardPage,
        UserResumePage,
    ],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule.forRoot(),
        TextMaskModule,
        ComponentModule,
        CardModule,
        HttpClientModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    providers: [],
})
export class UserInfoModule { }