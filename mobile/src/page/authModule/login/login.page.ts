import { Component, OnInit } from "@angular/core";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook/ngx";
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from "firebase/app";
import { LoginButtonArr } from "../../../models/loginButtonArr.interface";
import { LoginObjInterface } from "src/models/loginObj.interface";
import {
  NavController,
  LoadingController,
  ToastController,
  AlertController,
} from "@ionic/angular";
import { AuthService } from "src/services/auth.service";
import { config } from "src/services/config";
import { AngularFireDatabase } from "@angular/fire/database";
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  buttonArr: LoginButtonArr[] = [];
  isLoggedInFacebook = false;
  isUserSaveable = false;
  isPasswordType = false;
  loginObj: LoginObjInterface = {
    email: "",
    password: "",
  };
  constructor(
    public AuthSrv: AuthService,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    public toastController: ToastController,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.buttonArr = [
      {
        name: "Criar conta",
        icon: "mail",
        id: "email",
      },
      {
        name: "Entrar com Google",
        icon: "logo-google",
        id: "google",
      },
      {
        name: "Entrar com Facebook",
        icon: "logo-facebook",
        id: "facebook",
      },
    ];
  }
  async login(pathToFollow?) {
    const loading = await this.loadingController.create({
      duration: 2000,
      spinner: "bubbles",
    });
    await loading.present();
    switch (pathToFollow) {
      case "google":
        await this.AuthSrv.logInWithGoogle();
        loading.dismiss();
        break;
      case "email":
        this.navCtrl.navigateForward("auth/createuser");
        loading.dismiss();
        break;
      case undefined:
        await this.AuthSrv.logInWithEmail(this.loginObj);
        loading.dismiss();
        break;
      case "facebook":
        await this.AuthSrv.logInWithFacebook();
        loading.dismiss();
        break;
    }
  }

  async resetPassword() {
    const alert = await this.alertCtrl.create({
      header: "Insira o seu email para redefinir a sua senha",
      inputs: [
        {
          name: "emailReset",
          type: "email",
          placeholder: "Email para reset",
        },
      ],
      buttons: [
        {
          text: "Enviar",
          handler: (ev) => {
            console.log("Confirm Ok", ev);
            if (ev.emailReset !== "") {
              this.AuthSrv.resetUserPassword(ev.emailReset);
            }
          },
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: (ev) => {
            console.log("Confirm Cancel", ev);
          },
        },
      ],
    });

    await alert.present();
  }

  onClick() {
    /* this.AuthSrv.teste(); */
    this.navCtrl.navigateForward("home");
  }
}
