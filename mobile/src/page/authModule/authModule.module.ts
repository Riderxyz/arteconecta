import { DirectiveModule } from './../../directive/directive.module';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from './login/login.page';
import { CreateUserPage } from './createUser/create-user.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ComponentModule } from 'src/components/components.module';
import { AuthService } from 'src/services/auth.service';
import { GuardsService } from 'src/services/auth.guard';

const routes: Routes = [
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'createuser',
        component: CreateUserPage
    }
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DirectiveModule,
        ComponentModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    declarations: [
        LoginPage,
        CreateUserPage,
    ],
    providers: [
        GuardsService
    ],

    schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthModule { }