import { Component, OnInit, ViewChild } from '@angular/core';

import { NavController, IonSlide } from '@ionic/angular';
import { LocalStorageService } from 'src/services/localStorage.service';
import { config } from 'src/services/config';


@Component({
  selector: 'app-landing',
  templateUrl: 'landing.page.html',
  styleUrls: ['landing.page.scss'],
})
export class LandingPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    pagination: false
  };
  position = 0;
  backgroundColor = 'var(--ion-color-arteconecta-blue)';
  @ViewChild('landindPageSlide', { static: true }) slideElement: any;
  slideArr = [
/*     {
      img: './assets/splashScreens/landingPage1.png',
    },
    {
      img: './assets/splashScreens/landingPage2.png',
    },
    {
      img: './assets/splashScreens/landingPage3.png',
    },
    {
      img: './assets/splashScreens/landingPage4.png',
    }, */
  ];

  btnText = 'pular';
  constructor(
    private localSrv: LocalStorageService,
    private navCtrl: NavController,
  ) {
for (let i = 0; i < 4; i++) {
  
  this.slideArr.push({
    img:'./assets/splashScreens/landingPage' + (i + 1) + '.png',

  })
}
   }

  ngOnInit(): void {

  }

  async startEnd(ev) {
    this.position = await this.slideElement.getActiveIndex();
    
    const isEnd = await this.slideElement.isEnd();
    if (isEnd) {
      this.btnText = 'Continuar';
    } else {
      this.btnText = 'pular';
    }

  }

  async jumpToLogin() {
    await this.localSrv.setLocalData(config.localStorageKeys.alreadyPassedLandingPage, JSON.stringify(true));
    this.navCtrl.navigateRoot('auth/login');
  }
}
