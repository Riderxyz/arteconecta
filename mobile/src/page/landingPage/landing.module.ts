import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ComponentModule } from 'src/components/components.module';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';

import { LandingPage } from './landing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    RouterModule.forChild([
      {
        path: '',
        component: LandingPage
      }
    ])
  ],
  declarations: [LandingPage],
  providers: [YoutubeVideoPlayer]
})
export class LandingPageModule { }
