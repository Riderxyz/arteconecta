/* eslint-disable arrow-body-style */
import { BaseMaterialInterface } from './../../models/modeloNovo.interface';
import { ToastService } from './../../services/toast.service';
import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {
  ModalController,
  Platform,
  AnimationController,
  IonContent,
  NavController,
} from '@ionic/angular';
import { AuthService } from 'src/services/auth.service';
import { DataService } from 'src/services/data.service';
import { ModalInfoComponent } from 'src/components/modal-info/modalInfo.component';
import { BaseObjetivoInterface } from '../../models/modeloNovo.interface';
import {
  AtividadeInterface,
  hashValue,
} from './../../models/atividade.interface';
import {
  fadeInOnEnterAnimation,
  fadeOutOnLeaveAnimation,
  fadeInUpOnEnterAnimation,
  zoomInOnEnterAnimation,
  fadeInDownOnEnterAnimation,
  hingeOnLeaveAnimation,
} from 'angular-animations';
import * as lodash from 'lodash';
import { config } from 'src/services/config';
import { PushNotificationService } from 'src/services/pushNotification.service';
import { enterCustomAnimation } from 'src/components/modal-info/enterAnimation.animation';
import { leaveCustomAnimation } from 'src/components/modal-info/leaveAnimation.animation';
import { at } from 'lodash';
import { duration } from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  animations: [
    fadeInOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
    fadeInUpOnEnterAnimation(),
    fadeInDownOnEnterAnimation(),
    hingeOnLeaveAnimation(),
    zoomInOnEnterAnimation({ delay: 500 }),
  ],
})
export class HomePage implements OnInit, AfterContentInit {
  atividadeList: AtividadeInterface[] = [];
  displayList: AtividadeInterface[] = [];
  config = config;
  fabButton = {
    isActive: false,
  };
  arrLists = {
    atividadeList: [],
    displayList: [],
    videoUrl: [],
  };

  showOnlyFavorite = false;
  showLists = {
    showWithVideo: true,
    showWithoutVideo: true,
  };
  showLoad = false;
  objetivoList: BaseObjetivoInterface[] = [];
  materialList: BaseMaterialInterface[] = [];
  loadListUntil = 1000;
  previousLoadListUntil = this.loadListUntil;
  isSearchBarVisible = false;
  
  @ViewChild(IonContent , {static: false}) ionContent: IonContent;
  masks: any;
  phoneNumber: any = '';
  constructor(
    private authSrv: AuthService,
    private dataSrv: DataService,
    private modalCtrl: ModalController,
    private sanitizer: DomSanitizer,
    private pushSrv: PushNotificationService,
    private platform: Platform,
    private toastSrv: ToastService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    console.log('ENTREI NA PAGINA => H O M E');

    this.dataSrv.objetivoList.subscribe((objetivoList) => {
      this.objetivoList = objetivoList;
    });
    this.dataSrv.materialList.subscribe((materialList) => {
      this.materialList = materialList;
    });
    //this.showLoad = true;
    this.dataSrv.getData.subscribe((atividadeRes) => {
      this.atividadeList = [];
      this.displayList = [];
      this.showLoad = true;
      setTimeout(() => {
        if (this.platform.is('capacitor')) {
          this.pushSrv.getPhoneToken();
        }
      }, 300);
   for (let index = 0; index < atividadeRes.length; index++) {
     console.log(index);
     
    const atividadeObj = atividadeRes[index];
      if (atividadeObj.publicado) {
        const fixedUrl: any = this.sanitizer.bypassSecurityTrustUrl(
          'http://www.arteacaobrasil.com.br/adm/img/' + atividadeObj.imagem
        );
        atividadeObj.imagem = fixedUrl;
        let temp = [];
        let tempMaterial = [];
        let tamanho;
        try {
           tamanho = atividadeObj.lista_objetivo === undefined?0:atividadeObj.lista_objetivo.length;
        } catch (error) {
          tamanho = 0;
        }
        
        for (let indexObj = 0; indexObj < tamanho; indexObj++) {
          const element = atividadeObj.lista_objetivo[indexObj];
          temp.push(this.getObjetivos(element));
        }
        try {
          tamanho = atividadeObj.lista_material === undefined?0:atividadeObj.lista_material.length;
       } catch (error) {
         tamanho = 0;
       }
       tempMaterial = [];
        for (let indexMaterial = 0; indexMaterial < tamanho; indexMaterial++) {
          const element = atividadeObj.lista_material[indexMaterial];
          tempMaterial.push(this.getMateriais(element));
        }

        atividadeObj.lista_objetivo = temp;
        atividadeObj.lista_material = tempMaterial;
        this.atividadeList.push(atividadeObj);
        this.displayList.push(atividadeObj);
      }
    };
      // setTimeout(() => {
      //   console.log(this.atividadeList);
        this.showLoad = false;
      // }, 1000);
   });
   

      /*       this.atividadeList = atividadeRes;
      this.displayList = atividadeRes; */
  //  });
  }

  ngAfterContentInit(): void {
    // if (this.authSrv.currentUser) {
    // } else {
    //   console.log('23');
    // }
    //   setTimeout(() => {
    //  //   console.clear();
    //   }, 2000);
  }

  segmentChanged(ev) {
    if (ev === 'favoriteList') {
      this.showOnlyFavorite = true;
      this.loadListUntil = this.atividadeList.length;
    } else {
      this.showOnlyFavorite = false;
      this.loadListUntil = this.previousLoadListUntil;
    }
  }

  set searchBarCondition($event: boolean) {
    this.isSearchBarVisible = $event;
  }
  isFavorite(atividadeObj: AtividadeInterface) {
    if (this.authSrv.currentUser) {
      if (this.authSrv.currentUser.favoriteList === null) {
        this.authSrv.currentUser.favoriteList = [];
      }
      const retorno = lodash.includes(
        this.authSrv.currentUser.favoriteList,
        atividadeObj.id
      );
      return retorno;
    }
  }

  onFavorite(atividadeObj: AtividadeInterface, includeInList: boolean) {
    if (
      this.authSrv.currentUser.favoriteList === undefined ||
      this.authSrv.currentUser.favoriteList === null
    ) {
      this.authSrv.currentUser.favoriteList = [];
    }
    if (includeInList) {
      this.authSrv.currentUser.favoriteList.push(atividadeObj.id);
      this.dataSrv.saveTo(this.authSrv.currentUser, config.url.dbUser);
    } else {
      const index = this.authSrv.currentUser.favoriteList.indexOf(
        atividadeObj.id
      );
      if (index >= 0) {
        this.authSrv.currentUser.favoriteList.splice(index, 1);
        this.dataSrv.saveTo(this.authSrv.currentUser, config.url.dbUser);
      }
    }
  }

  changeShowList(
    isFavoriteTabOn: boolean,
    itemObj: AtividadeInterface,
    position: number
  ): boolean {
    // debugger;
    if (itemObj.publicado === true) {
      if (position < this.loadListUntil) {
        if (!(isFavoriteTabOn && !this.isFavorite(itemObj))) {
         /// // debugger;
          if (this.showLists.showWithoutVideo && this.showLists.showWithVideo) {
            return true;
          } else {
            if (this.showLists.showWithoutVideo) {
              return !itemObj.edicao;
            }
            if (this.showLists.showWithVideo) {
              return itemObj.edicao;
            }
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  showWichList(ev: {
    icon: string;
    name: string;
    selected: boolean;
    color: string;
    id: number;
  }) {

    // debugger;
/*     window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
}); */

    switch (ev.id) {
      case 2:
        this.showLists.showWithoutVideo = true;
        this.showLists.showWithVideo = false;
        this.ionContent.scrollToTop().finally(()=> {});
        break;
      case 1:
        this.showLists.showWithoutVideo = false;
        this.showLists.showWithVideo = true;
        this.ionContent.scrollToTop().finally(()=> {});
        break;
      case 0:
        this.showLists.showWithoutVideo = true;
        this.showLists.showWithVideo = true;
        this.ionContent.scrollToTop().finally(()=> {});
        break;
    }
  }

  getObjetivos(objetivoID) {
    const retorno = lodash.find(this.objetivoList, (lodashRes) => {
      return lodashRes.id === objetivoID;
    });
    if (retorno){
    return retorno.objetivo;
    } else {
      return [];
    }
  }
  getMateriais(materialID) {
    const retorno = lodash.find(this.materialList, (lodashRes) => {
      return lodashRes.id === materialID;
    });

    return retorno.material;
  }
  getMinMaxMaturidade(item: AtividadeInterface): string {
    if (!item.maturidadeMin) {
      return 'Livre para todas as idades';
    } else {
      if (item.maturidadeMax) {
        return 'De' + item.maturidadeMin + ' até ' + item.maturidadeMax;
      } else {
        return item.maturidadeMin + ' anos ou +';
      }
    }
  }
  async openModal(ev: AtividadeInterface, showVideo: boolean) {
    const modal = await this.modalCtrl.create({
      component: ModalInfoComponent,
      enterAnimation: enterCustomAnimation,
      leaveAnimation: leaveCustomAnimation,

      componentProps: {
        showVideo,
        videoObj: ev,
      },
    });
    if (!showVideo) {
      this.fabButton.isActive = !this.fabButton.isActive;
    }

    await modal.present();
    modal.onDidDismiss().then((res) => {
      if (!showVideo) {
        this.fabButton.isActive = !this.fabButton.isActive;
      }
    });
  }
  isImgAvailable(event, atividadeObj: AtividadeInterface) {
    atividadeObj.imagem = 'https://via.placeholder.com/150';
  }
  isProposta(atividadeObj: AtividadeInterface) {
   return !atividadeObj.edicao;
  }
  showInfiniteScroll() {
    let retorno = true;
    if (this.isSearchBarVisible) {
      retorno = false;
      return retorno;
    } else {
      if (this.showOnlyFavorite) {
        retorno = false;
        return retorno;
      }
      retorno = true;
      return retorno;
    }
  }
  loadNewAtividades(event) {
    if (!this.isSearchBarVisible) {
      // const lastDisplayPosition = this.displayList.length;
      setTimeout(async () => {
        this.loadListUntil += 15;
        this.previousLoadListUntil = this.loadListUntil;
        event.target.complete();
        if (this.loadListUntil >= this.atividadeList.length) {
          await this.toastSrv.showToast({
            header: 'Todo conteudo foi carregado',
            duration: 2000,
            position: 'bottom',
          });
          event.target.disabled = true;
        }
      }, 1500);
    }
  }
  onSearchtextChange(textValue: string) {
    this.showLoad = true;

    const result: AtividadeInterface[] = [];
// debugger;
    if (textValue !== '') {
      // eslint-disable-next-line prefer-const
      
      for (let element of this.atividadeList) {
        let h = hashValue(element, this.objetivoList, this.materialList);
        if (h.indexOf(textValue.toLowerCase().trim()) !== -1) {
          result.push(element);
        }
      }
      this.displayList = result;
    } else {
      this.displayList = this.atividadeList;
    }

    setTimeout(() => {
      this.showLoad = false;
    }, 500);
  }
}
