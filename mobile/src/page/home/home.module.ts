import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ComponentModule } from "src/components/components.module";
import { YoutubeVideoPlayer } from "@ionic-native/youtube-video-player/ngx";

import { HomePage } from "./home.page";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DirectiveModule } from 'src/directive/directive.module';
import { TextMaskModule } from 'angular2-text-mask';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    DirectiveModule,
    TextMaskModule,
    RouterModule.forChild([
      {
        path: "",
        component: HomePage,
      },
    ]),
  ],
  declarations: [HomePage],
  providers: [YoutubeVideoPlayer],
})
export class HomePageModule {}
