import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { config } from "./config";
import { PaymentResponseInterface } from "../models/paymentResponse.interface";
import { DataService } from "./data.service";
@Injectable()
export class PaymentService {
  constructor(private http: HttpClient, private dataSrv: DataService) {}
  makePayment(token, userId: string, costumerId: string) {
    return this.http.post<PaymentResponseInterface>(config.api.url.premium, {
      token,
      userId,
      costumerId,
    });
  }

  changeUserCreditCard(token, costumerId, userId) {
    return this.http.post(config.api.url.creditCard, {
      token,
      costumerId,
      userId,
    });
  }

  cancelPremium( subscribeId, userId) {
    return this.http.post(config.api.url.premiumCancel, {
        subscribeId,
        userId,
      });
  }
}
