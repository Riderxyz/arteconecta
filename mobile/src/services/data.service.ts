import { NavController } from '@ionic/angular';
import { AuthService } from './auth.service';
import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { config } from "./config";
import { AngularFireDatabase } from "@angular/fire/database";
import * as lodash from "lodash";
import {
  BaseObjetivoInterface,
  BaseMaterialInterface,
} from "./../models/modeloNovo.interface";
import {
  AtividadeInterface
} from "./../models/atividade.interface";
import {
  UsuarioInteface
} from "./../models/usuario.interface";
import { ToastService } from "./toast.service";
import { environment } from "../environments/environment";

@Injectable()
export class DataService {
  listaObjetivos: BaseObjetivoInterface[] = [];
  listaMateriais: BaseMaterialInterface[] = [];
  constructor(
    private httpClient: HttpClient,
    public db: AngularFireDatabase,
    private toastSrv: ToastService,
    private navCtrl: NavController
  ) {
    this.objetivoList.subscribe((res) => {
      this.listaObjetivos = res;
    });

    this.materialList.subscribe((res) => {
      this.listaMateriais = res;
    });
    
  }

  getFavoriteAtividade(urlEvent) {
    return this.db
    .object<AtividadeInterface>(config.url.base + config.url.dbAtividades + '/' + urlEvent)
    .valueChanges();
  }
  get getData() {
    return this.db
      .list<AtividadeInterface>(config.url.base + config.url.dbAtividades)
      .valueChanges();
  }
  get objetivoList() {
    return this.db
      .list<BaseObjetivoInterface>(config.url.base + config.url.dbObjetivos)
      .valueChanges();
  }
  get materialList() {
    return this.db
      .list<BaseMaterialInterface>(config.url.base + config.url.dbMateriais)
      .valueChanges();
  }
  get getUsuariosList() {
    return this.db
      .list<UsuarioInteface>(config.url.base + config.url.dbUser)
      .valueChanges();
  }
  getSpecificUser(id: string) {
    return this.db
      .object<UsuarioInteface>(
        config.url.base + config.url.dbUser + "/" + id
      )
      .valueChanges();
  }

  DecodeObjectList(objetivoID) {
    const retorno = lodash.find(this.listaObjetivos, (lodashRes) => {
      return lodashRes.id === objetivoID;
    });
    return retorno.objetivo;
  }

  DecodeMateriaisList(MaterialID) {
    const retorno = lodash.find(this.listaMateriais, (lodashRes) => {
      return lodashRes.id === MaterialID;
    });
    return retorno.material;
  }

  set deleteFromDB(id: string) {
    this.db.list(config.url.base + config.url.dbUser).remove(id);
  }
  public saveTo(ev: any, path: string) {
    this.db
      .list(config.url.base + path)
      .set(ev.id, ev)
      .then(() => {
        if (path === config.url.dbUser) {
          /* this.localSrv.setLocalData(config.localStorageKeys.userData, ev) */
          this.toastSrv.showToastSucess('Seus dados foram salvos');
          //this.navCtrl.navigateBack('userInfo/userResume')
        }
      });
  }

  sendHelp(messageText, userEmail, userName) {

    var headers = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
      };

      var parm = {
        nomeUsuario:userName,
        emailUsuario:userEmail, 
        textoFeedback: messageText,
      };

      console.log('PARAMETRO DO HTTP ',parm);

    return this.httpClient.post(config.api.url.sendEmail, parm, headers);
  }
}
