import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { LoginObjInterface } from "src/models/loginObj.interface";
import { NavController } from "@ionic/angular";
import { config } from "./config";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook } from "@ionic-native/facebook/ngx";
import * as firebase from "firebase/app";
import { DataService } from "./data.service";
import { ToastService } from "./toast.service";
import { AuthExtended } from "./authExtended.service";
@Injectable()
export class AuthService extends AuthExtended {
  // tslint:disable-next-line: variable-name

  constructor(
    private afAuth: AngularFireAuth,
    private gplus: GooglePlus,
    private fb: Facebook,
    public toastSrv: ToastService,
    private navCtrl: NavController,
    public dataSrv: DataService,
  ) {
    super(dataSrv, toastSrv);
    setTimeout(() => { }, 10000);
  }
  get currentUserObservable() {
    return this.afAuth.authState;
  }
  SignUp(userObj: LoginObjInterface) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(userObj.email, userObj.password)
      .then(async (dados) => {
        const res: any = dados.user;
        const additionalUserInfo = dados;
        this.getUserData(additionalUserInfo);
        this.toastSrv.showToastSucess("Conta criada com sucesso!");
      })
      .catch(async (error) => {
        console.log(error);
        let msg = "Não foi possivel criar a sua conta.";
        if (error.code == "auth/email-already-in-use")
         msg = "Já existe um usuário com este email!"
        this.toastSrv.showToastError(
          msg,
          "Por favor, tente novamente"
        );
        // await loading.dismiss();
      });
  }
  async logInWithEmail(userObj: LoginObjInterface) {
    const path = firebase.auth.Auth.Persistence.LOCAL;
    // debugger;
    this.afAuth.auth
      .setPersistence(path)
      .then(async () => {
        try {
          let loginNormal = null;
            loginNormal = await this.afAuth.auth.signInWithEmailAndPassword(
              userObj.email,
              userObj.password
            );
          if (loginNormal !== null) {
            await this.getUserData(loginNormal);
          } else {

          }
        } catch (error) {
          console.log('erro de login ', error)
          console.log('msg erro login => ', config.errorMensages.defaultLoginErrorMessage(userObj.email))
          this.toastSrv.showToastError(
            'Não foi possivel autenticar. Por favor, verifique o usuário: ' + userObj.email + error + ' e a senha e tente novamente'
          );
          this.errorAuthHandler(error);
          this.navCtrl.navigateRoot("/auth/login");
          this.LogOut();
        }
      })
      .catch((error) => {
        this.toastSrv.showToastError(
          config.errorMensages.defaultLoginErrorMessage(userObj.email),
          "Por favor, tente novamente"
        );
      });
  }
  async logInWithGoogle() {
    const path = firebase.auth.Auth.Persistence.LOCAL;
    this.afAuth.auth
      .setPersistence(path)
      .then(async () => {
        try {
          const gplusUser = await this.gplus.login({
            webClientId:
              "1018276036038-mapep0u8vfd4jmmhb8f67s7n86do1cfe.apps.googleusercontent.com",
            offline: true,
            // scopes: "profile email",
          });
          const googleAuth = await this.afAuth.auth.signInWithCredential(
            firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken)
          );
          if (typeof googleAuth.user.uid !== undefined && googleAuth.user.uid) {
            /* await this.localSrv.setLocalData(
              config.localStorageKeys.refreshToken,
              googleAuth.user.refreshToken
            ); */
            await this.getUserData(googleAuth);
          }
        } catch (err) {
          this.errorAuthHandler(err);
        }
      })
      .catch((err) => {
        this.toastSrv.showToastError(
          config.errorMensages.defaultLoginErrorMessage("Google"),
          "Por favor, tente novamente " + err
        );
      });
  }


  /* "auth/email-already-in-use" */
  async logInWithFacebook() {
    const path = firebase.auth.Auth.Persistence.LOCAL;
    this.afAuth.auth
      .setPersistence(path)
      .then(async () => {
        try {
          const fplususer = await this.fb.login(["public_profile", "email"]);
          const facebookAuth = await this.afAuth.auth.signInWithCredential(
            firebase.auth.FacebookAuthProvider.credential(
              fplususer.authResponse.accessToken
            )
          );
          if (
            typeof facebookAuth.user.uid !== undefined &&
            facebookAuth.user.uid
          ) {
            /*             await this.localSrv.setLocalData(
                          config.localStorageKeys.refreshToken,
                          facebookAuth.user.refreshToken
                        ); */
            await this.getUserData(facebookAuth);
          }
        } catch (err) {
          this.errorAuthHandler(err);
        }
      })
      .catch((err) => {
        this.errorAuthHandler(err);
        this.toastSrv.showToastError(
          config.errorMensages.defaultLoginErrorMessage("Facebook"),
          "Por favor, tente novamente "+ err
        );
      });
  }
  resetUserPassword(email) {
    this.afAuth.auth
      .sendPasswordResetEmail(email)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }
  deletarConta() {
    this.afAuth.authState.subscribe((res) => {
      this.dataSrv.deleteFromDB = res.uid;
      res.delete();
      this.toastSrv.showToastSucess("Sua conta foi deletada com sucesso");
    });
  }

  errorAuthHandler(errorEvent: firebase.auth.AuthError) {
    switch (errorEvent.code) {
      case config.errorAuthCodes.accountExistsWithDifferentCredential:
        this.toastSrv.showToastError(
          config.errorMensages.accountExistsWithDifferentCredentialMessage
        );
        break;
      case config.errorAuthCodes.emailAlreadyExists:
        this.toastSrv.showToastError(config.errorMensages);
        break;
    }
  }
  LogOut() {
    this.afAuth.auth.signOut().then((resAuth) => {
      console.log("Deslogado");
      this.deleteLocalUserData();
      this.navCtrl.navigateRoot("auth/login");
    });
  }
}
