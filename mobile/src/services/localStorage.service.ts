import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
@Injectable()
export class LocalStorageService {
    constructor(private storage: Storage) { }
    async getLocalData(key: string) {
        return await this.storage.get(key);
    }

    async setLocalData(key: string, value: string) {
        await this.storage.set(key, value);
    }

    async whipeData(key) {
        await this.storage.remove(key);
    }
    async whipeAllData() {
        await this.storage.clear();
    }
}
