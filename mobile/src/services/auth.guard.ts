import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthService } from './auth.service';
import { take, map, tap } from 'rxjs/operators';
import { ToastService } from './toast.service';


@Injectable()
export class GuardsService implements CanActivate {
    public isAuthenticated = false;
    constructor(
        public afAuth: AngularFireAuth,
        private AuthSrv: AuthService,
        private router: Router,
        private toastSrv: ToastService
         ) {
    }
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
            return this.AuthSrv.currentUserObservable
              .pipe(
                take(1),
                map((user) => {
                  return !!user;
                }), tap(loggedIn => {
                  if (!loggedIn) {
                    console.log('access denied');
                    this.router.navigateByUrl('auth/login');
                  }
                })
              );
          }
}