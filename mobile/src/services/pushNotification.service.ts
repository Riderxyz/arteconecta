
import { Injectable } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { DataService } from './data.service';
import * as lodash from 'lodash';
import { AuthService } from './auth.service';
import { config } from './config';

@Injectable()
export class PushNotificationService {
    currentPhoneToken = '';
    constructor(
        private firebaseX: FirebaseX,
        private DataSrv: DataService,
        private AuthSrv: AuthService) {
    }
    getPhoneToken() {
        console.log('EU EXISTO AQUI');
        return this.firebaseX.getToken()
            .then((token) => {
                this.currentPhoneToken = token;
                if (typeof this.AuthSrv.currentUser.notificationTokenList === undefined) {
                    this.AuthSrv.currentUser.notificationTokenList = [];
                }
                const retorno = lodash.includes(
                    this.AuthSrv.currentUser.notificationTokenList,
                    this.currentPhoneToken
                );
                if (retorno) {
                    console.log('Ja existe');
                } else {
                    console.log(this.AuthSrv.currentUser.notificationTokenList = []);
                    this.AuthSrv.currentUser.notificationTokenList.push(this.currentPhoneToken);
                    this.DataSrv.saveTo(this.AuthSrv.currentUser, config.url.dbUser);
                }
            })
            .catch((error) => {
                console.error('Error getting token', error);
            }
            );
    }
}