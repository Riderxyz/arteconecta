import { estados } from "./estados";

export const config = {
  localStorageKeys: {
    refreshToken: "refreshToken",
    passedTutorial: "passedTutorial",
    userData: "userData",
    alreadyPassedLandingPage: "alreadyPassedLandingPage",
  },
  url: {
    base: "", //"/ArteConecta",
    dbUser: "/usuario",
    dbMateriais: "/material",
    dbObjetivos: "/objetivo",
    dbAtividades: "/atividades",
  },
  iconsFromFirebase: {
    objetivo:
      "https://firebasestorage.googleapis.com/v0/b/arteconecta-27292.appspot.com/o/icons%2Fobjetivos-tela-principal.png?alt=media&token=5bb3a370-4da3-4528-91c7-7645f38ea631",
    materiais:
      "https://firebasestorage.googleapis.com/v0/b/arteconecta-27292.appspot.com/o/icons%2Fmaterial-tela-principal.png?alt=media&token=907b67b8-8ab0-48f1-8d42-6609468ae757",
    faixaEtaria:
      "https://firebasestorage.googleapis.com/v0/b/arteconecta-27292.appspot.com/o/icons%2Ffaixa-etaria-tela-principal.png?alt=media&token=5dea8818-c7f0-4d17-b1ca-03a989db94f4",
    individuo:
      "https://firebasestorage.googleapis.com/v0/b/arteconecta-27292.appspot.com/o/icons%2Findividual-tela-principal.png?alt=media&token=64125b76-e8cc-4a44-8b55-232d36b4a514",
    descricao:
      "https://firebasestorage.googleapis.com/v0/b/arteconecta-27292.appspot.com/o/icons%2Fdescricao-tela-principal.png?alt=media&token=61b954c8-e740-4548-84f1-f85ec5d042d3",
    pais: 
    "https://firebasestorage.googleapis.com/v0/b/arteconecta-27292.appspot.com/o/icons%2Fpais-tela-principal.png?alt=media&token=cc009115-d18d-4484-b61a-0a8c9d4f5195",
  },
  api: {
    url: {
      securetoken: "https://securetoken.googleapis.com/v1/token?key=",
      premium:
        "https://us-central1-arteconecta-27292.cloudfunctions.net/onUserPremiumRequest",
      premiumCancel:
        "https://us-central1-arteconecta-27292.cloudfunctions.net/onUserPremiumDelete",
      creditCard:
        "https://us-central1-arteconecta-27292.cloudfunctions.net/onUserCreditCardChange",
      createUserCostumerId:
        "https://us-central1-arteconecta-27292.cloudfunctions.net/userWithoutCostumerIDnn",
      sendEmail:
        "https://us-central1-arteconecta-27292.cloudfunctions.net/onEmailRequest",
    },
    key: {
      main: "AIzaSyDNm35wQZ1WrNc_x3YUW5I_qeuSBtZyYuY",
      youtube: "AIzaSyDTxpZ_OJ0PAElrlhDwRCc1pi1EsvLPZnk",
      stripeKey: "pk_test_72rPIkL3xloJZ45tf1Xe6Yxj00k6v9XtzR",
    },
    canal: [
      {
        id: "UCaIztg6vcfpRKk7gsxjyGmA",
        nome: "Arte e ação",
      },
    ],
  },
  errorAuthCodes: {
    emailAlreadyExists: "auth/email-already-exists",
    accountExistsWithDifferentCredential:
      "auth/account-exists-with-different-credential",
    invalidPhoneNumber: "auth/invalid-phone-number",
  },
  errorMensages: {
    defaultLoginErrorMessage: (usuario: string) => {
      return;
      "Não foi possivel autenticar. Por favor, verifique o usuário: " +
        usuario +
        " e senha e tente novamente";
    },
    accountExistsWithDifferentCredentialMessage:
      "Não foi possivel entrar na sua conta pois o endereço de email pois ele ja ",
  },

  estados,
};
