import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core/dist/types/components/toast/toast-interface';
@Injectable()
export class ToastService {
  constructor(private toastController: ToastController) { }

  async showToast(config: ToastOptions) {
    const toast = await this.toastController.create(config);
    toast.present();
  }
  async showToastSucess(title, subtitle?) {
    if (subtitle === undefined) {
      subtitle = null;
    }
    const sucessToast = await this.toastController.create({
      header: title,
      message: subtitle,
      position: 'bottom',
      cssClass: 'toast-success',
      translucent: true,
      keyboardClose: true,
      duration: 1500,
    });
    sucessToast.present();
  }
  async showToastError(title, subtitle?) {
    if (subtitle === undefined) {
      subtitle = null;
    }
    const errorToast = await this.toastController.create({
      header: title,
      message: subtitle,
      position: 'bottom',
      cssClass: 'toast-danger',
      translucent: true,
      keyboardClose: true,
/*       showCloseButton: true,
      closeButtonText: 'X', */
      duration: 5000,
    });
    errorToast.present();
  }
}
