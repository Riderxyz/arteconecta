import { AtividadeInterface } from 'src/models/atividade.interface';
import { UsuarioInteface } from 'src/models/usuario.interface';
 export interface ArteConectaInterface {
    usuarios: UsuarioInteface[];
    atividades: AtividadeInterface[];
    base_material: BaseMaterialInterface[];
    base_objetivo: BaseObjetivoInterface[];
}

export interface BaseObjetivoInterface {
    id?: string;
    objetivo?: string;
}

export interface BaseMaterialInterface {
    id?: string;
    material?: string;
}


