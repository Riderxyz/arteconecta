export interface FirebaseYoutubeDataBase {
    title: string;
    videoId: string;
    thumbnail: string;
    deatails: string;
}
export interface YouTubeReqInterface {
    kind: string;
    etag: string;
    nextPageToken: string;
    regionCode: string;
    pageInfo: YouTubePageInfoInterface;
    items: YouTubeItemInterface[];
}

export interface YouTubeItemInterface {
    kind: string;
    etag: string;
    id: YouTubeIdInterface;
    snippet: YouTubeSnippetInterface;
}

export interface YouTubeSnippetInterface {
    publishedAt: string;
    channelId: string;
    title: string;
    description: string;
    thumbnails: YouTubeThumbnailsInterface;
    channelTitle: string;
    liveBroadcastContent: string;
}

export interface YouTubeThumbnailsInterface {
    default: YouTubeDefaultInterface;
    medium: YouTubeDefaultInterface;
    high: YouTubeDefaultInterface;
}

export interface YouTubeDefaultInterface {
    url: string;
    width: number;
    height: number;
}

export interface YouTubeIdInterface {
    kind: string;
    videoId: string;
}

export interface YouTubePageInfoInterface {
    totalResults: number;
    resultsPerPage: number;
}



export interface ArtigosInterface {
    atualizadoPor: string;
    autor: string;
    comentario: string;
    sugestaoComentario: string;
    data: string;
    dataHoraAtualizacao: string;
    idArtigo: string;
    imageLink: string;
    subTitulo: string;
    isFavorite?: boolean;
    texto: string;
    titulo: string;
    urlOriginal: string;
  }