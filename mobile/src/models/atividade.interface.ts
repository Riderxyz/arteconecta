import { BaseMaterialInterface, BaseObjetivoInterface } from "./modeloNovo.interface";
import * as lodash from "lodash";
export interface AtividadeInterface {
    id?: string;
    origem: string;
    tipo?: 'VideoAula' | 'Proposta';
    titulo?: string;
    lista_atividade: string[];
    lista_material: string[];
    lista_objetivo: string[];
    maturidadeMin?: number;
    maturidadeMax?: number;
    numeroParticipantes?: string;
    descricao?: string;
    sugestoes?: string;
    comentarios?: string;
    legendasVideo?: string;
    kit: Kit;
    imagem?: string;
    videourl?: string;
    edicao?: boolean//string;
    numedicao?: number;
    indicacaopais?: boolean;
    free?: boolean;
    partipantes?: string;
    revisao?: boolean;//string;
    publicado?: boolean;//string;
    excluido?: boolean;
    atividadesassociadas: Atividadesassociada[];
    update_data?: Date;
    update_autor?: string;
    update_tipo?: string;
    update_observacao?: string;
    data_criacao: Date;
    criadopor: string;
}

export function hashValue(a: AtividadeInterface, listaobjetivo?: BaseObjetivoInterface[], listaMateriais?: BaseMaterialInterface[]): string {
    var result: string = a.titulo.toLocaleLowerCase() + ' ';
    result += a.descricao.toLocaleLowerCase() + ' ';
    //result += a.comentarios.toLocaleLowerCase() + ' ';
    result += ajustaArrayMaterial(a.lista_material, listaMateriais);
    result += concatenarArrayString(a.lista_objetivo).toLocaleLowerCase();// ajustaArrayObjetivo(a.lista_objetivo, listaobjetivo).toLocaleLowerCase();
    result += concatenarArrayString(a.lista_atividade).toLocaleLowerCase();
    result += a.maturidadeMin != null ? a.maturidadeMin.toString() + ' anos ' : " ";
    result += a.maturidadeMax != null ? a.maturidadeMax.toString() + ' anos ' : " ";
    result += a.partipantes != null ? a.partipantes.toString() + ' ' : " ";
    result += a.numeroParticipantes != null ? a.numeroParticipantes.toString() + ' ' : " ";
    result += a.tipo + ' ';
    result += a.indicacaopais ? 'pais' : ' ';
    return result;
}

function concatenarArrayString(lista: string[]): string {
    var result = "";
    for (const key in lista) {
        const element = lista[key];
        result += element + " ";
    }
    return result;
}

function ajustaArrayObjetivo(objetivos: string[], lista: BaseObjetivoInterface[]): string {
    try {
        if (objetivos == undefined || objetivos == null) {
            return ''
        } else {
            var result = [];
            for (let index = 0; index < objetivos.length; index++) {
                const element = lista[index];
                var res = lodash.find(lista, (a: BaseObjetivoInterface) => {
                    return a.id === element.id
                });
                result.push(res.objetivo);
            }
            console.log(result.join(';'));
            return result.join(';');
        }
    } catch (error) {
        return '';
    }

}

function ajustaArrayMaterial(material: string[], lista: BaseMaterialInterface[]): string {
    //console.log('LIsta de materiais ', material, lista);
    try {
        if (material == undefined || material == null) {
            return ''
        } else {
            var result = [];
            for (let index = 0; index < material.length; index++) {
                const element = lista[index];
                var res = lodash.find(lista, (a: BaseMaterialInterface) => {
                    return a.id === element.id
                });
                result.push(res.material);
            }
          //  console.log(result.join(';'));
            return result.join(';');
        }
    } catch (error) {
        return '';
    }
}



export interface Atividadesassociada {
    associacaoatividadeid?: string;
    dataassociacao?: Date;
    associadapor?: string;
}

export interface Kit {
    tipo: Tipo[];
}

export interface Tipo {
    descricaoKit?: string;
    materiais?: string;
    instrucoes?: string;
}
