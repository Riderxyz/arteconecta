import { Component, OnInit, Input } from '@angular/core';
import { StripeCardTokenParams, StripeCardTokenRes } from '@ionic-native/stripe/ngx';
import { HttpClient } from "@angular/common/http";
import { config } from '../../services/config';
import { LoadingController, ModalController } from '@ionic/angular';
import { PaymentService } from 'src/services/payment.service';
import { ToastService } from 'src/services/toast.service';
import { AuthService } from 'src/services/auth.service';
@Component({
  templateUrl: './modal-checkout.component.html',
  styleUrls: ['./modal-checkout.component.scss']
})
export class ModalCheckoutComponent implements OnInit {

  vantagens = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna  aliqua.',
    '  Egestas sed sed risus pretium quam vulputate dignissim suspendisse.',
    'Pellentesque dignissim enim sit amet venenatis urna cursus eget.',
    'Risus nec feugiat in fermentum posuere.',
    'Morbi non arcu risus quis varius quam quisque'
  ]
    ;
  @Input() checkOutObj: StripeCardTokenParams;
  @Input() token: StripeCardTokenRes;
  constructor(
    private http: HttpClient,
    private loadingController: LoadingController,
    private paymentSrv: PaymentService,
    private toastSrv: ToastService,
    private modalController: ModalController,
    private AuthSrv: AuthService
  ) { }

  ngOnInit() {

  }

  async makePayment() {
    const loading = await this.loadingController.create({
      message: '',
      spinner: 'bubbles'
    });
    await loading.present();
    this.paymentSrv.makePayment(this.token.id, this.AuthSrv.currentUser.id, this.AuthSrv.currentUser.premium.costumerId)
      .subscribe(async (res) => {
        await loading.dismiss();
        this.toastSrv.showToastSucess('Parabens, você agora é um usuario Premium');
        this.modalController.dismiss();
      }, (async (err) => {
        await loading.dismiss();
        this.modalController.dismiss();
        this.toastSrv.showToastError('Não foi possivel concluir o pagamento', 'tente novamente mais tarde')
      }));
  }
  close() {
    this.modalController.dismiss();
  }
}

