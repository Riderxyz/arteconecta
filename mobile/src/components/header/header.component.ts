import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import {
  slideOutUpOnLeaveAnimation,
  zoomInOnEnterAnimation,
  zoomOutUpOnLeaveAnimation
} from "angular-animations";
import { IonSearchbar } from '@ionic/angular';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  animations: [zoomInOnEnterAnimation(), slideOutUpOnLeaveAnimation(), zoomOutUpOnLeaveAnimation()],
})
export class HeaderComponent implements OnInit {
  @Input() nomeDaPagina: string;
  @Input() isAtHome: boolean = false;
  @Output() TabChange = new EventEmitter();
  @Output() isSearchBarVisible = new EventEmitter<boolean>();
  /* @ViewChild('searchBar', {static: true}) searchBar: IonSearchbar; */
  @Output() searchOutput = new EventEmitter<string>()
  selectedTab: string = "allList";
  isSearchActive: boolean = false;
  searchValue: string = "";
  constructor() {}

  ngOnInit() {
    this.TabChange.emit(this.selectedTab);
  }
  

  changeTab(event) {
    this.TabChange.emit(this.selectedTab);
  }
  showSearchBar() {
   this.isSearchActive = !this.isSearchActive;
   (this.isSearchActive);
   this.isSearchBarVisible.emit(this.isSearchActive);
    
  }

  clearSearch(event){
    this.isSearchActive = !this.isSearchActive;
    this.searchValue = ''; 
    this.searchOutput.emit(this.searchValue);
    
  }
  onValueChange() {
    this.searchOutput.emit(this.searchValue);
  }


}
