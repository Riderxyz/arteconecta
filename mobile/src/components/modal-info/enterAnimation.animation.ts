import { Animation } from "@ionic/core";
import { createAnimation } from "@ionic/core";
import { AnimationController } from "@ionic/angular";
const ex = AnimationController;
import { AppInjector } from "../../app/app.module";

export const enterCustomAnimation = (baseEl: HTMLElement): Animation => {
  const animationCtrl = AppInjector.get(AnimationController);

  const backdropAnimation = animationCtrl
    .create()
    .addElement(baseEl.querySelector("ion-backdrop")!)
    .fromTo("opacity", "0.01", "var(--backdrop-opacity)");

  const wrapperAnimation = animationCtrl
    .create()
    .addElement(baseEl.querySelector(".modal-wrapper")!)
    .keyframes([
      { offset: 0, opacity: "0", transform: "scale(0)" },
      { offset: 1, opacity: "0.99", transform: "scale(1)" },
    ]);

  return animationCtrl
    .create()
    .addElement(baseEl)
    .easing("ease-out")
    .duration(500)
    .delay(100)
    .addAnimation([backdropAnimation, wrapperAnimation]);
};
