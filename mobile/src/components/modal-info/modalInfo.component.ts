import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  OnDestroy,
} from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ModalController } from "@ionic/angular";
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from "@angular/animations";
import { AtividadeInterface } from "src/models/atividade.interface";
import { DataService } from "src/services/data.service";
import { config } from "src/services/config";

@Component({
  selector: "app-modalYt",
  templateUrl: "./modalInfo.component.html",
  styleUrls: ["./modalInfo.component.scss"],
  animations: [
    trigger("enterTrigger", [
      state(
        "fadeIn",
        style({
          opacity: "1",
          transform: "translateY(3%)",
        })
      ),
      transition("void => *", [style({ opacity: "0" }), animate("500ms")]),
    ]),
  ],
})
export class ModalInfoComponent implements OnInit, AfterViewInit, OnDestroy {
  tabArr: {
    title: string;
    id: string | number;
    isActive?: boolean;
  }[] = [];
  config = config;
  @Input() videoObj: AtividadeInterface;
  @Input() showVideo: boolean;
  url = "https://www.youtube.com/embed/";
  trustYT: SafeResourceUrl;
  descricaoFormatedArr: {
    title: string;
    value: string;
  }[] = [];
  constructor(
    private sanitizer: DomSanitizer,
    public dataSrv: DataService,
    public modalCtrl: ModalController
  ) {
   
  }

  ngOnInit(): void {
    this.isListEmpty();
    
    console.log(this.videoObj);
  }
  ngAfterViewInit() {
   
    const setPadding: any = document.getElementsByClassName("show-modal");

    if (this.showVideo) {
      const changeDiv: any = document.getElementsByClassName(
        "modal-wrapper"
      )[0];
      changeDiv.style.height = "50%";
      changeDiv.style.background = "transparent";
    }
    this.formatDescricao();
  }
  getMinMaxMaturidade(): string {
    if (!this.videoObj.maturidadeMin) {
      return "Livre para todas as idades";
    } else {
      if (this.videoObj.maturidadeMax) {
        return (
          this.videoObj.maturidadeMin +
          " a " +
          this.videoObj.maturidadeMax
        );
      } else {
        return this.videoObj.maturidadeMin + ' anos ou +';
      }
    }
  }

  async close() {
    await this.modalCtrl.dismiss();
  }
  getTrustedURl() {
    const newUrl = this.url + this.videoObj.videourl;
    return this.sanitizer.bypassSecurityTrustResourceUrl(newUrl);
  }
  isImgAvailable(event) {
    this.videoObj.imagem = "https://via.placeholder.com/150";
  }

  changeToVideo() {
    this.showVideo = !this.showVideo;
  }
  formatDescricao() {
    const arr = this.videoObj.descricao.split(/([0-9]-+)/);
    if (arr.length < 2) {
      this.descricaoFormatedArr.push({
        title: '',
        value: this.videoObj.descricao
      })
      
    }
    for (let i = 0; i < arr.length; i++) {
      if (i !== 0) {
        if (i % 2 !== 0) {
          this.descricaoFormatedArr.push({
            title: arr[i],
            value: arr[i + 1],
          });
        }
      }
    }
  }

  isListEmpty() {
   // debugger;
    if (this.videoObj.lista_material) {
      this.videoObj.lista_material.forEach((element, index) => {
        element = element.replace(/\s/g, '')
        if (element.length <=0) {
          this.videoObj.lista_material.splice(index, 1)
        }
      });
    }
    if ( this.videoObj.lista_objetivo) {
      this.videoObj.lista_objetivo.forEach((element, index) => {
        element = element.replace(/\s/g, '')
        if (element.length <=0) {
          this.videoObj.lista_objetivo.splice(index, 1)
        }
      });
    }
  }

  isProposta() {
    if (!this.videoObj.edicao) {
      return true;      
    }
    return false;
  }

  ngOnDestroy() {
    const setPadding: any = document.getElementsByTagName("ion-modal")[0];
    if (setPadding) {
      setPadding.style.padding = "";
    }

    if (this.showVideo) {
      const changeDiv: any = document.getElementsByClassName(
        "modal-wrapper"
      )[0];
      if (changeDiv) {
        changeDiv.style.height = "";
        changeDiv.style.background = "";
      }
    }
  }
}
