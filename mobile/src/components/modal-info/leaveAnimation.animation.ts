import { Animation } from "@ionic/core";

import { enterCustomAnimation } from './enterAnimation.animation';

export const leaveCustomAnimation = ((baseEl: HTMLElement): Animation => {
  return enterCustomAnimation(baseEl).direction('reverse');
});
