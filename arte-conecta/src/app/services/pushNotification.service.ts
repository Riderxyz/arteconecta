import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import * as lodash from 'lodash';
import { AuthService } from './auth.service';
import { config } from './config';
import { PushNotifications } from '@capacitor/push-notifications';

@Injectable()
export class PushNotificationService {
  currentPhoneToken = '';
constructor(private dataSrv: DataService, private authSrv: AuthService) {}


async getPhoneToken() {

await this.registerNotifications();
  console.log('EU EXISTO AQUI');
    await PushNotifications.addListener('registration', (token) => {
      this.currentPhoneToken = token.value;
      if (typeof this.authSrv.currentUser.notificationTokenList === undefined) {
        this.authSrv.currentUser.notificationTokenList = [];
      }
      const retorno = lodash.includes(
        this.authSrv.currentUser.notificationTokenList,
        this.currentPhoneToken
      );
      if (retorno) {
        console.log('Ja existe');
      } else {
        console.log((this.authSrv.currentUser.notificationTokenList = []));
        this.authSrv.currentUser.notificationTokenList.push(
          this.currentPhoneToken
        );
        this.dataSrv.saveTo(this.authSrv.currentUser, config.url.dbUser);
      }
    });

    await PushNotifications.addListener('registrationError', (err) => {
      console.error('Registration error: ', err.error);
    });

    await PushNotifications.addListener(
      'pushNotificationReceived',
      (notification) => {
        console.log('Push notification received: ', notification);
      }
    );

    await PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification) => {
        console.log(
          'Push notification action performed',
          notification.actionId,
          notification.inputValue
        );
      }
    );
  }

  async registerNotifications() {
    let permStatus = await PushNotifications.checkPermissions();

    if (permStatus.receive === 'prompt') {
      permStatus = await PushNotifications.requestPermissions();
    }

    if (permStatus.receive !== 'granted') {
      throw new Error('User denied permissions!');
    }

    await PushNotifications.register();
  }

  async getDeliveredNotifications() {
    const notificationList = await PushNotifications.getDeliveredNotifications();
    console.log('delivered notifications', notificationList);
  }
}
