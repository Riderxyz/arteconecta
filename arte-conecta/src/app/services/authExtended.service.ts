/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
import { ToastService } from './toast.service';
import { DataService } from './data.service';
import { UsuarioInteface } from '../models/usuario.interface';
import { config } from './config';
export class AuthExtended {
  config = config;
  private _currentUser: UsuarioInteface = null;
  private _isNewUser = null;
  constructor(
    public dataSrv: DataService,
    public toastSrv: ToastService,
  ) {}
  public get currentUser(): UsuarioInteface {
    return this._currentUser;
  }

  public set currentUser(v: UsuarioInteface) {
    this._currentUser = v;
  }

  async getUserData(loginData: firebase.default.auth.UserCredential) {
    if (loginData.additionalUserInfo) {
      if (loginData.additionalUserInfo.isNewUser) {
        this.toastSrv.showToastError(
          'Você não possui uma conta neste App.',
          ' '
        );
        this.setNewUserData(loginData.user);
      }
    } else {
      await this.dataSrv
        .getSpecificUser(loginData.user.uid)
        .subscribe((user) => {
          this.currentUser = user;
          this.currentUser.novoUsuario = false;
          this.dataSrv.saveTo(this.currentUser, config.url.dbUser);
        });
    }
    //JrKZHHS8IAWK7y7EdKgGdgCG9zB2
  }
  setNewUserData(event: firebase.default.User) {

    const user = event;
    const newUser: UsuarioInteface = {
      id: user.uid,
      nome:   user.displayName === '' || user.displayName === undefined  ? user.displayName : 'Nome Provisório ' + user.email,
      emailPrincipal: user.email,
      celular: user.phoneNumber,
      /*             premium: {
              status: false,
              costumerId: costumer.id
            }, */
      dt_cadastro: '' + new Date() + '',
      updateDate: '' + new Date() + '',
      profissao: ' ',
      outraProfissao: '',
      updateBy: user.displayName === '' || user.displayName === undefined ? user.email : user.displayName,
      novoUsuario: true,
    };
    if (user.displayName !== '' || user.displayName !== null) {
      newUser.nome = user.displayName;
    } else {
      newUser.nome = 'Nome Provisório ' + user.email;
    }
    this.currentUser = newUser;

     this.dataSrv.saveTo(newUser, config.url.dbUser);
    // this.navCtrl.navigateRoot('userInfo/userDetail');
  }

  deleteLocalUserData() {
    this.currentUser = null;
  }
}
