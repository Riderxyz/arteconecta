import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LazyLoadingImgDirective } from './Lazy-Loading-Img/lazy-loading-img.directive';
import { MustMatchDirective } from './must-mach/must-match.directive';
@NgModule({
  declarations: [
      LazyLoadingImgDirective,
      MustMatchDirective
    ],
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      IonicModule
    ],
  exports: [
      LazyLoadingImgDirective,
      MustMatchDirective
    ],
  providers: [],
})
export class DirectiveModule {}
