import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
/* import { GuardsService } from 'src/services/guard.service'; */
import { GuardsService } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'landingPage', pathMatch: 'full' },
  {
    path: 'home',
    canActivate: [GuardsService],
    loadChildren: () =>
      import('./page/home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'userInfo',
    canActivate: [GuardsService],
    loadChildren: () =>
      import('./page/userInfo/userInfo.module').then((m) => m.UserInfoModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./page/authModule/authModule.module').then((m) => m.AuthModule),
  },
  {
    path: 'ajuda&Feedback',
    canActivate: [GuardsService],
    loadChildren: () =>
      import('./page/ajudaFeedback/ajudaFeedback.module').then(
        (m) => m.AjudaFeedbackModule
      ),
  },
  {
    path: 'sobre',
    canActivate: [GuardsService],
    loadChildren: () =>
      import('./page/about/about.module').then((m) => m.AboutModule),
  },
/*   {
    path: 'landingPage',
    loadChildren: () =>
      import('./page/landingPage/landing.module').then(
        (m) => m.LandingPageModule
      ),
  }, */
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
