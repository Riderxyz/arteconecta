/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginObjInterface } from '../../../models/loginObj.interface';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AuthService } from '../../../services/auth.service';
import { NavController, LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-user',
  templateUrl: 'create-user.page.html',
  styleUrls: ['./create-user.page.scss'],
})
export class CreateUserPage implements OnInit {
  userObj: LoginObjInterface;
  aceitaTermos = false;
  @ViewChild('RegisterForm', { static: true }) RegisterForm: NgForm;
  constructor(
    private AuthSrv: AuthService,
    private navCtrl: NavController,
    public loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.userObj = {
      email: null,
      password: null,
      confirmPassword: null,
    };
  }

  async onRegister() {
    const loading = await this.loadingController.create({
      duration: 2000,
      spinner: 'bubbles',
    });
    await loading.present();
    await this.AuthSrv.SignUp(this.userObj);
    await loading.dismiss;
  }
  isFormValid(form: NgForm) {
    let retorno = true;
    if (form.form.valid) {
      if (this.aceitaTermos) {
        retorno = false;
      }
    }
    return retorno;
  }
  cancelar() {
    this.navCtrl.navigateBack('auth/login');
  }
}
