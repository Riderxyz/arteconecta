import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import * as firebase from 'firebase/app';
import { LoginButtonArr } from '../../../models/loginButtonArr.interface';
import { LoginObjInterface } from '../../../models/loginObj.interface';
import {
  NavController,
  LoadingController,
  ToastController,
  AlertController,
} from '@ionic/angular';
import { AuthService } from '../../../services/auth.service';
import { config } from '../../../services/config';
import { AngularFireDatabase } from '@angular/fire/compat/database';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  buttonArr: LoginButtonArr[] = [];
  isLoggedInFacebook = false;
  isUserSaveable = false;
  isPasswordType = false;
  loginObj: LoginObjInterface = {
    email: '',
    password: '',
  };
  constructor(
    public authSrv: AuthService,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    public toastController: ToastController,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.buttonArr = [
      {
        name: 'Criar conta',
        icon: 'mail',
        id: 'email',
      },
      {
        name: 'Entrar com Google',
        icon: 'logo-google',
        id: 'google',
      },
      {
        name: 'Entrar com Facebook',
        icon: 'logo-facebook',
        id: 'facebook',
      },
    ];
  }
  async login(pathToFollow?) {
    const loading = await this.loadingController.create({
      duration: 2000,
      spinner: 'bubbles',
    });
    await loading.present();
    switch (pathToFollow) {
      case 'google':
        await this.authSrv.logInWithGoogle();
        loading.dismiss();
        break;
      case 'email':
        this.navCtrl.navigateForward('auth/createuser');
        loading.dismiss();
        break;
      case undefined:
        await this.authSrv.logInWithEmail(this.loginObj);
        loading.dismiss();
        break;
      case 'facebook':
      ;//  await this.authSrv.logInWithFacebook();
        loading.dismiss();
        break;
    }
  }

  async resetPassword() {
    const alert = await this.alertCtrl.create({
      header: 'Insira o seu email para redefinir a sua senha',
      inputs: [
        {
          name: 'emailReset',
          type: 'email',
          placeholder: 'Email para reset',
        },
      ],
      buttons: [
        {
          text: 'Enviar',
          handler: (ev) => {
            console.log('Confirm Ok', ev);
            if (ev.emailReset !== '') {
              this.authSrv.resetUserPassword(ev.emailReset);
            }
          },
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (ev) => {
            console.log('Confirm Cancel', ev);
          },
        },
      ],
    });

    await alert.present();
  }

  onClick() {
    /* this.AuthSrv.teste(); */
    this.navCtrl.navigateForward('home');
  }
}
