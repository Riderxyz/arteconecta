/* eslint-disable @typescript-eslint/member-ordering */
import {
  Component,
  AfterViewInit,
  OnInit,
} from '@angular/core';
/* import { config } from "../../../services/config"; */
/* import { PushNotificationService } from "../../../services/pushNotification.service"; */
import { AuthService } from '../../../services/auth.service';
import { UsuarioInteface } from './../../../models/usuario.interface';
import * as moment from 'moment';
import { NavController, AlertController } from '@ionic/angular';
import {
  fadeInOnEnterAnimation,
  fadeOutOnLeaveAnimation,
  fadeInUpOnEnterAnimation,
} from 'angular-animations';
@Component({
  templateUrl: './userResume.page.html',
  styleUrls: ['./userResume.page.scss'],
  animations: [
    fadeInOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
    fadeInUpOnEnterAnimation(),
  ],
})
export class UserResumePage implements AfterViewInit, OnInit {
  userData: UsuarioInteface;
  showLoad = false;
  showPrice = false;
  premiumPrice = '16,90';
  constructor(
    private authSrv: AuthService,
    public navCtrl: NavController,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.userData = this.authSrv.currentUser;
  }

  ngAfterViewInit() {
/*     this.userData.dt_nascimento = moment(this.userData.dt_nascimento).format(
      "L"
    );
    console.log(this.userData.dt_cadastro);
    const dataCadastro = moment(this.userData.dt_cadastro).format("L");
    if (dataCadastro !== "Invalid Date") {
      this.userData.dt_cadastro = moment(this.userData.dt_cadastro).format("L");
    } */
  }

  get isUserPremium() {
    if (this.userData.premium.status) {
      return true;
    } else {
      return false;
    }
  }

  get getPremiumTime(): number {
    const premiumDate = new Date(
      this.userData.premium.dt_ativacaoPremium
    ).getTime();
    const today = new Date().getTime();
    const diferencaEmTempo = today - premiumDate;
    const result = diferencaEmTempo / (1000 * 3600 * 24);
    return Math.round(result);
  }

  /*   get isPushAvailable(): boolean {
      if (this.userData.notificationTokenList === undefined) {
        return false;
      } else {
        return true;
      }
    }
    onPushTokenRequest(event) {
      console.log('Olaaaa', event.detail.checked);
      if (event.detail.checked) {
        this.pushSrv.getPhoneToken().then(() => {
          console.log('token adicionado', this.authSrv.currentUser);
        });
    }
  } */
}
