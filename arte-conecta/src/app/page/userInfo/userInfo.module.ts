import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ComponentModule } from '../../components/components.module';
import { UserEditPage } from './userDetail/userEdit.page';
import { UserResumePage } from './userResume/userResume.page';
import {  HttpClientModule } from '@angular/common/http';

const routes: Routes = [
    {
        path: 'userDetail',
        component: UserEditPage
    },
    {
        path: 'userResume',
        component: UserResumePage
    },
];
@NgModule({
    declarations: [
        UserEditPage,
        UserResumePage,
    ],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule.forRoot(),
        ComponentModule,
        HttpClientModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    providers: [],
})
export class UserInfoModule { }
