import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss'],
})
export class AboutPage implements OnInit {
  constructor(public navCtrl: NavController) {}

  ngOnInit() {}

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }
}
