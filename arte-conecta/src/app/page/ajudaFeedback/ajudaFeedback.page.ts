import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { AuthService } from '../../services/auth.service';

@Component({
  templateUrl: 'ajudaFeedback.page.html',
  styleUrls: ['ajudaFeedback.page.scss'],
})
export class AjudaFeedbackPage implements OnInit {
  helpText = '';
  activate = false;
  tabName = '';
  constructor(
    private dataSrv: DataService,
    private authSrv: AuthService,
    private toastSrv: ToastService,
    private loadingController: LoadingController,
    public navCtrl: NavController,
  ) {}

  ngOnInit() {}

  async sendHelp(form) {
      const loading = await this.loadingController.create({
        message: 'Enviando mensagem',
        spinner: 'bubbles'
      });
      await loading.present();
    this.dataSrv
      .sendHelp(
        this.helpText,
        this.authSrv.currentUser.emailPrincipal,
        this.authSrv.currentUser.nome
      )
      .subscribe(async (res) => {
          await loading.dismiss();
          this.toastSrv.showToastSucess(
            'Sua mensagem foi enviada',
            'Agradecemos pelo FeedBack'
          );
        },
        async (err) => {
          await loading.dismiss();
          this.toastSrv.showToastSucess(
            'Sua mensagem foi enviada',
            'Agradecemos pelo FeedBack'
          );
        }
      );
  }
  getSelectedTab(ev) {
    this.tabName = ev;
  }
}
