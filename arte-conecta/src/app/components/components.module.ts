import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { ModalInfoComponent } from './modal-info/modalInfo.component';
import { LoaderComponent } from './loader/loader.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsComponent } from './tabs/tabs.component';
import { ExpandableSearchComponent } from './expandable-search/expandable-search.component';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [
    ModalInfoComponent,
    LoaderComponent,
    HeaderComponent,
    TabsComponent,
    ExpandableSearchComponent,
  ],
  declarations: [
    ModalInfoComponent,
    HeaderComponent,
    LoaderComponent,
    TabsComponent,
    ExpandableSearchComponent,
  ],
  entryComponents: [ModalInfoComponent],
  providers: [],
})
export class ComponentModule {}
