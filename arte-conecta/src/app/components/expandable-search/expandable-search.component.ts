/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable no-underscore-dangle */
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  forwardRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
@Component({
  selector: 'app-expandable-search',
  templateUrl: './expandable-search.component.html',
  styleUrls: ['./expandable-search.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ExpandableSearchComponent),
    },
  ],
})
export class ExpandableSearchComponent implements OnInit, ControlValueAccessor {
  @Input() disabled = false;
  inputModel = '';
  private _value: string;
  inputName = () => Math.random().toString(36).substr(2, 9);
  inputId = () => Math.random().toString(36).substr(2, 9);

  onChange: any = () => {};
  onTouched: any = () => {};
  constructor() {}
  ngOnInit(): void {}

  public get value() {
    return this._value;
  }

  public set value(v) {
    this._value = v;
    this.onChange(this._value);
    this.onTouched();
  }

  writeValue(obj: any): void {
    this._value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
