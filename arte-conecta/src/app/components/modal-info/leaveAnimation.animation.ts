import { Animation } from '@ionic/core';

import { enterCustomAnimation } from './enterAnimation.animation';

export const leaveCustomAnimation = ((baseEl: HTMLElement): Animation => enterCustomAnimation(baseEl).direction('reverse'));
