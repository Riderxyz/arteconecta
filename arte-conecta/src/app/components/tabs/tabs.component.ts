/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit, AfterViewInit {
  @Output() TabChange = new EventEmitter();
  arrButton = [
    {
      icon: 'apps-sharp',
      name: 'Propostas',
      selected: false,
      color:'#c2c2c2',
      id: 2,
    },
    {
      icon: 'person-sharp',
      name: 'Videoaulas',
      selected: false,
      color:'#c2c2c2',
      id: 1,
    },
    {
      icon: 'notifications',
      name: 'Todas',
      selected: true,
      color:'#d04980',
      id: 0,
    },
  ];
  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {

    const xpto: any = document.getElementsByClassName('tabs-inner')[0];
    xpto.style.display = 'none';
  }

  onTabClick(ev) {
    for (let i = 0; i < this.arrButton.length; i++) {
      const element = this.arrButton[i];
      element.selected = false;
      element.color = '#c2c2c2';
      if (ev.id === element.id) {
        element.selected = true;
        element.color = '#d04980';
        this.TabChange.emit(ev);
      }
    }
  }
}
