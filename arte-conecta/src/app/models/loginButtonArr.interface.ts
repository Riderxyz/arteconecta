export interface LoginButtonArr {
    name: string;
    icon: string;
    id: string;
}