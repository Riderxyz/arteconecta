/* eslint-disable @typescript-eslint/naming-convention */
export interface PaymentResponseInterface {
    id: string;
    object: string;
    amount: number;
    amount_refunded: number;
    application?: any;
    application_fee?: any;
    application_fee_amount?: any;
    balance_transaction: string;
    billing_details: BillingDetailsInterface;
    calculated_statement_descriptor: string;
    captured: boolean;
    created: number;
    currency: string;
    customer?: any;
    description: string;
    destination?: any;
    dispute?: any;
    disputed: boolean;
    failure_code?: any;
    failure_message?: any;
    fraud_details: FraudDetailsInterface;
    invoice?: any;
    livemode: boolean;
    metadata: FraudDetailsInterface;
    on_behalf_of?: any;
    order?: any;
    outcome: OutcomeInterface;
    paid: boolean;
    payment_intent?: any;
    payment_method: string;
    payment_method_details: PaymentMethodDetailsInterface;
    receipt_email?: any;
    receipt_number?: any;
    receipt_url: string;
    refunded: boolean;
    refunds: RefundsInterface;
    review?: any;
    shipping?: any;
    source: SourceInterface;
    source_transfer?: any;
    statement_descriptor?: any;
    statement_descriptor_suffix?: any;
    status: string;
    transfer_data?: any;
    transfer_group?: any;
  }

  interface SourceInterface {
    id: string;
    object: string;
    address_city?: any;
    address_country?: any;
    address_line1?: any;
    address_line1_check?: any;
    address_line2?: any;
    address_state?: any;
    address_zip?: any;
    address_zip_check?: any;
    brand: string;
    country: string;
    customer?: any;
    cvc_check: string;
    dynamic_last4?: any;
    exp_month: number;
    exp_year: number;
    fingerprint: string;
    funding: string;
    last4: string;
    metadata: FraudDetailsInterface;
    name: string;
    tokenization_method?: any;
  }

  interface RefundsInterface {
    object: string;
    data: any[];
    has_more: boolean;
    total_count: number;
    url: string;
  }

  interface PaymentMethodDetailsInterface {
    card: PaymentCardInterface;
    type: string;
  }

  interface PaymentCardInterface {
    brand: string;
    checks: ChecksInterface;
    country: string;
    exp_month: number;
    exp_year: number;
    fingerprint: string;
    funding: string;
    installments?: any;
    last4: string;
    network: string;
    three_d_secure?: any;
    wallet?: any;
  }

  interface ChecksInterface {
    address_line1_check?: any;
    address_postal_code_check?: any;
    cvc_check: string;
  }

  interface OutcomeInterface {
    network_status: string;
    reason?: any;
    risk_level: string;
    risk_score: number;
    seller_message: string;
    type: string;
  }

  interface FraudDetailsInterface {
  }

  interface BillingDetailsInterface {
    address: AddressInterface;
    email?: any;
    name: string;
    phone?: any;
  }

  interface AddressInterface {
    city?: any;
    country?: any;
    line1?: any;
    line2?: any;
    postal_code?: any;
    state?: any;
  }
