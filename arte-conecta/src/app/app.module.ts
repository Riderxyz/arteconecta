import { HttpClientModule } from '@angular/common/http';

/* eslint-disable @typescript-eslint/naming-convention */
import { environment } from './../environments/environment';
import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
//Modules
import { ComponentModule } from './components/components.module';
import { DirectiveModule } from './directive/directive.module';


//services
import { GuardsService } from './services/auth.guard';
import { PushNotificationService } from './services/pushNotification.service';
import { ToastService } from './services/toast.service';
import { AuthService } from './services/auth.service';
import { CleanerService } from './services/cleaner.service';
import { DataService } from './services/data.service';

/* import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { provideDatabase, getDatabase } from '@angular/fire/database';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { enableIndexedDbPersistence } from 'firebase/firestore'; */
//environment

/* const AngularFire = [
  provideFirestore(() => {
    const firestore = getFirestore();
    enableIndexedDbPersistence(firestore);
    return firestore;
  }),
  provideAuth(() => getAuth()),
  provideDatabase(() => getDatabase()),
]; */
export let AppInjector: Injector;
const FormModules = [ReactiveFormsModule, FormsModule];
const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule,
];
const Modules = [ComponentModule, DirectiveModule];
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ...AngularFire,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    ...FormModules,
    ...Modules,
  ],
  providers: [
    DataService,
    CleanerService,
    AuthService,
    ToastService,
    GuardsService,
    PushNotificationService,

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private injector: Injector) {
    AppInjector = this.injector;
  }
}
