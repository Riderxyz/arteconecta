import { DataService } from './services/data.service';
import { Component } from '@angular/core';
import { Platform, NavController, MenuController } from '@ionic/angular';
import { AuthService } from './services/auth.service';
import { StatusBar, Style } from '@capacitor/status-bar';
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError,
} from '@angular/router';
import { CleanerService } from './services/cleaner.service';
import { config } from './services/config';
import * as firebase from 'firebase/app';
import * as moment from 'moment';
import { timer } from 'rxjs';

import {
  zoomInOnEnterAnimation,
  bounceOutOnLeaveAnimation,
} from 'angular-animations';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  arrBtn =  [
    {
      name: 'Início',
      path: '/home',
      icon: 'home',
    },
    {
      name: 'Sobre',
      path: '/sobre',
      icon: 'information-circle',
    },
    {
      name: 'Sua Conta',
      path: '/userInfo/userResume',
      icon: 'finger-print',
    },
    /*         {
              name: 'Premium',
              path: '/userInfo/userCreditCard',
              icon: 'star'
            }, */
    {
      name: 'ajuda & feedback',
      path: '/ajuda&Feedback',
      icon: 'help-circle',
    },
    {
      name: 'sair',
      path: '',
      icon: 'exit',
    },
  ];;
  showSplash = true;
  constructor(
    private platform: Platform,
    /* private splashScreen: SplashScreen, */
    private menuCtrl: MenuController,
    public navCtrl: NavController,
    public authSrv: AuthService,
    private dataSrv: DataService,
    private router: Router
  ) {
    this.initializeApp();
    router.events.subscribe((val: Event) => {
      if (val instanceof NavigationEnd) {
        if (val.url === '/auth/login') {
          this.isMenuEnable(false);
        } else {
          this.isMenuEnable(true);
        }
      }
    });
    this.authSrv.currentUserObservable.subscribe(async (user) => {
      if (user) {
        // await this.isUserDataAvailable(user);
        this.dataSrv.getSpecificUser(user.uid).subscribe((res) => {
          if (res === null) {
            //    this.AuthSrv.currentUser = res
            this.authSrv.setNewUserData(user);
            this.navCtrl.navigateRoot('userInfo/userDetail');
          } else {
            this.authSrv.currentUser = res;
            if (this.authSrv.currentUser.novoUsuario) {
              this.navCtrl.navigateRoot('userInfo/userDetail');
            } else {
              this.navCtrl.navigateRoot('/home');
            }
          }
        });
      } else {
          this.navCtrl.navigateRoot('auth/login');
      }
    });
    this.platform.backButton.subscribe(async () => {
      if (
        (this.router.isActive('/home', true) && this.router.url === '/home') ||
        (this.router.isActive('/auth/login', true) &&
          this.router.url === '/auth/login')
      ) {
        // tslint:disable-next-line: no-string-literal
        // eslint-disable-next-line @typescript-eslint/dot-notation
        navigator['app'].exitApp();
      }
    });
  }



  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.hide();
      GoogleAuth.initialize({
        clientId: '1018276036038-mapep0u8vfd4jmmhb8f67s7n86do1cfe.apps.googleusercontent.com',
        scopes: ['profile', 'email'],
        grantOfflineAccess: true,
      });
      timer(4000).subscribe(() => (this.showSplash = false));
    });
  }

  async isMenuEnable(value) {
    this.menuCtrl.enable(value);
  }
  goTo(event) {
    if (event.name === 'sair') {
      this.authSrv.LogOut();
    } else {
      this.navCtrl.navigateRoot(event.path);
    }
  }
}
