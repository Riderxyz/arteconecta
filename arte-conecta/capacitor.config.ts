/* eslint-disable @typescript-eslint/naming-convention */
import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'arteConecta',
  webDir: 'www',
  bundledWebRuntime: false,
  plugins: {
    GoogleAuth: {
      scopes: ['profile', 'email'],
      clientId:'1018276036038-mapep0u8vfd4jmmhb8f67s7n86do1cfe.apps.googleusercontent.com',
      serverClientId: '1018276036038-mapep0u8vfd4jmmhb8f67s7n86do1cfe.apps.googleusercontent.com',
      forceCodeForRefreshToken: true,
    },
  },
};

export default config;
